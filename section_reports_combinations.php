<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 03:43 PM
 */
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quatro Combinations</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <p>
                <form>
                    <label for="dateFrom">FROM</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label for="dateFrom">TO</label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">

                    <input type="submit" value="Show" class="button" onclick="enviarFechas(); return false"/>
                </form>

                <div id="result">

                    </p>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>

    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });


    function enviarFechas(){
        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val()
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_action.php',
            type: 'post',
            beforeSend: function(){
                $("#result").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#result").html(response);
            }
        });
    }




</script>