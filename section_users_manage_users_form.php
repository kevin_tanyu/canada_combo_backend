<?php
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

/*NIVELES DE USUARIOS*/
$sqlNivelUsuarios = "SELECT * FROM UsuariosNivel";
$stmtNivelUsuarios = $pdoConn->prepare($sqlNivelUsuarios);
$stmtNivelUsuarios->execute();
$nivelUsuarios = $stmtNivelUsuarios->fetchAll(PDO::FETCH_ASSOC);

/*USUARIOS POR NIVEL*/
$sqlUsers = "SELECT * FROM Usuarios
             WHERE NivelUsuario = ?
             ORDER BY IDPadre ASC";
$stmtUsers = $pdoConn->prepare($sqlUsers);

/*USUARIOS POR IDPADRE*/
$sqlUsersParent = "SELECT * FROM Usuarios
                   WHERE ID = ?";
$stmtUsersParent = $pdoConn->prepare($sqlUsersParent);

/*GET PERMISOS*/
$sqlGetUserAction = "SELECT * FROM Menus_Accion_Usuarios
                     WHERE idAccion = ? AND usuarioID = ?";
$stmtGetUserAction = $pdoConn->prepare($sqlGetUserAction);

/*PERMISOS USUARIOS*/
$stmtGetUserAction->execute(array(5, $_SESSION['IDUsuario']));
$addAgent = $stmtGetUserAction->fetch();

$stmtGetUserAction->execute(array(6, $_SESSION['IDUsuario']));
$addStore = $stmtGetUserAction->fetch();

$stmtGetUserAction->execute(array(7, $_SESSION['IDUsuario']));
$addMachine = $stmtGetUserAction->fetch();

$showAdd = false;

?>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Users Group &nbsp;&nbsp;&nbsp;&nbsp;
                <a data-toggle="modal" data-target="#myModalNewUser" id="aAdduser"><i class="fa fa-plus-circle" id="btnAddUser"></i> Add User</a>
            </div>
            <!-- Modal ADD USER-->
            <div class="modal fade" id="myModalNewUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Add New User</h4>
                        </div>
                        <div class="modal-body">*
                            <label>Username</label>
                            <input class="form-control" style="width: 230px" id="txtNewUsername">*
                            <label>Password</label>
                            <input class="form-control" style="width: 230px" id="txtNewPassword">
                            <label>Email</label>
                            <input class="form-control" style="width: 230px" id="txtNewEmail">
                            <label>Percentage</label>
                            <input class="form-control" style="width: 230px" id="txtNewPercentage">
                            *<label>User Group</label>
                            <select class="form-control" style="width: 230px" id="slcNewUserGroup" onchange="getGroupSelect()">
                                <?php foreach($nivelUsuarios as $nivel):?>
                                <?php if($_SESSION['NivelUsuario'] != 1){?>
                                    <?php if($addAgent['activo'] == 1 && $nivel['id'] == 2):?>
                                     <option value="<?php echo $nivel['id']?>"><?php echo $nivel['jerarquia']?></option>
                                    <?php endif?>
                                    <?php if($addStore['activo'] == 1 && $nivel['id'] == 3):?>
                                        <option value="<?php echo $nivel['id']?>"><?php echo $nivel['jerarquia']?></option>
                                    <?php endif?>
                                    <?php if($addMachine['activo'] == 1 && $nivel['id'] == 4):?>
                                        <option value="<?php echo $nivel['id']?>"><?php echo $nivel['jerarquia']?></option>
                                    <?php endif?>
                                 <?php }else{ ?>
                                        <option value="<?php echo $nivel['id']?>"><?php echo $nivel['jerarquia']?></option>
                                 <?php } ?>
                                <?php endforeach?>
                            </select>
                            <div id="divSlcParent"></div>
                            </br>
                            <div id="divCreateMsg" class="text-warning"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button id="btnAddNewUser" type="button" class="btn btn-primary" onclick="createUser()">Create User</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- .panel-heading -->
            <div class="panel-body">
                <div class="panel-group" id="accordion">
                    <?php foreach($nivelUsuarios  as $nivel):?>
                    <?php if($addAgent['activo'] == 1 && $nivel['id'] == 2 || $addStore['activo'] == 1 && $nivel['id'] == 3 || $addMachine['activo'] == 1 && $nivel['id'] == 4 || $_SESSION['NivelUsuario'] == 1):?>
                    <?php $showAdd = true;?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $nivel['id'];?>"><?php echo $nivel['jerarquia'];?></a>&nbsp;&nbsp;

                            </h4>
                        </div>
                        <div id="collapse<?php echo $nivel['id'];?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php $stmtUsers->execute(array($nivel['id']));
                                      $Users = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);
                                       ?>
                                <div class="panel-body">
                                <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Email</th>
                                    <th><?php echo $nivel['padre'];?></th>
                                    <th>%</th>
                                    <th></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($Users as $user):?>
                                <tr class="odd gradeX">
                                    <td><?php echo $user['NombreUsuario']?></td>
                                    <td><?php echo $user['Contrasena']?></td>
                                    <td><?php echo $user['Email']?></td>
                                    <?php $stmtUsersParent->execute(array($user['IDPadre']));
                                          $parent = $stmtUsersParent->fetch();
                                    ?>
                                    <td><?php echo $parent['NombreUsuario']?></td>
                                    <td><?php echo $user['Porcentaje']?></td>
                                    <td></td>
                                    <td class="center">
                                        <div class="tooltip-demo">
                                          <button type="button" class="btn btn-primary btn-circle" data-toggle="modal" data-target="#myModalEdit<?php echo $user['ID']?>"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit User"></i>
                                          </button>
                                          <?php if($nivel['id'] != 1):?>
                                            <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#myModalDelete<?php echo $user['ID']?>"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete User"></i>
                                            </button>
                                          <?php endif ?>
                                        </div>
                                    </td>
                                </tr>
                                    <!-- Modal EDIT-->
                                    <div class="modal fade" id="myModalEdit<?php echo $user['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit User: <?php echo $user['NombreUsuario']?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <label>Password</label>
                                                    <input class="form-control" style="width: 230px" id="txtPassword<?php echo $user['ID']?>" value="<?php echo $user['Contrasena']?>">
                                                    <label>Email</label>
                                                    <input class="form-control" style="width: 230px" id="txtEmail<?php echo $user['ID']?>" value="<?php echo $user['Email']?>">
                                                    <label>Percentage %</label>
                                                    <input class="form-control" style="width: 230px" id="txtPercentage<?php echo $user['ID']?>" value="<?php echo $user['Porcentaje']?>">
                                                    <?php if($nivel['id'] != 1):?>
                                                        <label>Belongs to the <?php echo $nivel['padre']?></label>
                                                        <select class="form-control" style="width: 230px" id="slcFather<?php echo $user['ID']?>">
                                                            <?php $stmtUsers->execute(array($user['NivelUsuario']-1));//IMPORTANTE!!!!!!!!!!!!!
                                                            $UsersParent = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);?>
                                                        <?php foreach($UsersParent as $userParent):?>
                                                            <?php if($user['IDPadre'] == $userParent['ID']){?>
                                                              <option value="<?php echo $userParent['ID']?>" selected><?php echo $userParent['NombreUsuario']?></option>
                                                            <?php }else{ ?>
                                                              <option value="<?php echo $userParent['ID']?>"><?php echo $userParent['NombreUsuario']?></option>
                                                            <?php }?>
                                                        <?php endforeach ?>
                                                        </select>
                                                    <?php endif ?>
                                                    </br>
                                                    <div id="divEditMsg<?php echo $user['ID']?>" class="text-info"></div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button id="btnEdit<?php echo $user['ID']?>" type="button" class="btn btn-primary" onclick="updateUser(<?php echo $user['ID']?>)">Save Changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal EDIT-->

                                    <!-- Modal DELETE-->
                                    <div class="modal fade" id="myModalDelete<?php echo $user['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4 class="text-warning">Are you sure to delete the user?</h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <button id="btnDelete<?php echo $user['ID']?>" type="button" class="btn btn-danger" onclick="deleteUser(<?php echo $user['ID']?>)">Yes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal DELETE -->
                                <?php endforeach ?>
                                <!-- USERS FOREACH -->
                                </tbody>
                                </table>
                                </div>
                                <!-- /.table-responsive -->

                                </div>
                                <!-- /.panel-body -->

                            </div>
                        </div>
                    </div>
                        <?php endif?>
                    <?php endforeach ?>
                </div>
            </div>
            <!-- .panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script>
    $(document).ready(function() {
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })

        <?php if($showAdd == false):?>
            $("#btnAddUser").css('visibility', 'hidden');
            $("#aAdduser").css('visibility', 'hidden');
        <?php endif?>
    });

    //GROUP SELECT
    function getGroupSelect(){
        if($('#slcNewUserGroup').val() != 1){
            var parametros = {
                "ParentLevel" : $('#slcNewUserGroup').val()-1,
                "Accion" : "ParentGroup"
            }
            $.ajax({
                data: parametros,
                url: 'section_users_manage_users_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#divSlcParent").html("</br>Please.. Wait a moment");
                },
                success: function(response){
                    if(response == 'ERROR'){
                        $("#divSlcParent").html("A error has ocurred. Please try again");
                        $("#btnAddUser").attr("data-dismiss", "modal");

                    }else{
                        $("#divSlcParent").html(response);
                    }
                }
            });
        }else{
            $("#divSlcParent").html('');
        }


    }//GROUP SELECT

    //CREATE USER
    function createUser(){

        if($('#txtNewUsername').val() != '' && $('#txtNewPassword').val() != '' && $('#slcNewUserGroup').val() != null){
            $("#btnAddNewUser").attr("data-dismiss", "modal");
            if($('#slcNewUserGroup').val() == 1){
                var parentID = 0;
            }else{
                var parentID = $('#slcNewParent').val();
            }
            var parametros = {
                "Username" : $('#txtNewUsername').val(),
                "Password" : $('#txtNewPassword').val(),
                "Email" : $('#txtNewEmail').val(),
                "Percentage" : $('#txtNewPercentage').val(),
                "UserGroup" : $('#slcNewUserGroup').val(),
                "ParentID" : parentID,
                "Accion" : "Create"
            }
            $.ajax({
                data: parametros,
                url: 'section_users_manage_users_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#divCreateMsg").html("Please.. Wait a moment");
                },
                success: function(response){
                    if(response == 'ERROR'){
                        //$("#divEditMsg" + userID).html("A error has ocurred. Please try again");
                        alert("A error has ocurred. Please try again");

                    }else{
                        //  $("#divEditMsg" + userID).html(response);
                        alert(response);
                    }
                    //RECARGA EL DIV DE USUARIOS
                    $("#divUserGroup").load("section_users_manage_users_form.php");

                }
            });
        }else{
            $("#divCreateMsg").html("Fill empty spaces");
        }


    }//CREATE USER

    //EDITUSER
    function updateUser(userID){

        $("#btnEdit" + userID).attr("data-dismiss", "modal");
        var parametros = {
            "userID" : userID,
            "Password" : $('#txtPassword' + userID).val(),
            "Email" : $('#txtEmail' + userID).val(),
            "Percentage" : $('#txtPercentage' + userID).val(),
            "Parent" : $('#slcFather' + userID).val(),
            "Accion" : 'Update'
        };
        $.ajax({
            data: parametros,
            url: 'section_users_manage_users_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divEditMsg" + userID).html("Please.. Wait a moment");
            },
            success: function(response){
                if(response == 'ERROR'){
                    //$("#divEditMsg" + userID).html("A error has ocurred. Please try again");
                    alert("A error has ocurred. Please try again");

                }else{
                  //  $("#divEditMsg" + userID).html(response);
                    alert(response);
                }
                //RECARGA EL DIV DE USUARIOS
                    $("#divUserGroup").load("section_users_manage_users_form.php");



            }
        });

    }//EDITUSER


    //DELETEUSER
    function deleteUser(userID){

        $("#btnDelete" + userID).attr("data-dismiss", "modal");
        var parametros = {
            "userID" : userID,
            "Accion" : 'Delete'
        };
        $.ajax({
            data: parametros,
            url: 'section_users_manage_users_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divEditMsg" + userID).html("Please.. Wait a moment");
            },
            success: function(response){
                if(response == 'ERROR'){
                    //$("#divEditMsg" + userID).html("A error has ocurred. Please try again");
                    alert("A error has ocurred. Please try again");

                }else{
                    //  $("#divEditMsg" + userID).html(response);
                    alert(response);
                }
                //RECARGA EL DIV DE USUARIOS
                $("#divUserGroup").load("section_users_manage_users_form.php");



            }
        });
    }//DELETEUSER


</script>
