<?php

$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

$userID = $_SESSION['IDUsuario'];

?>



<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Balance</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <form>
                    <label>FROM</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label>TO</label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">

                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosHouse)):?>
                        <div id="divAgent"></div>
                        <div id="divStore"></div>
                        <div id="divMachine"></div>
                    <?php endif ?>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosAgent)):?>
                        <td>
                            <div id="divStore"></div>
                            <div id="divMachine"></div>
                        </td>
                    <?php endif ?>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosStore)):?>
                        <td>
                            <div id="divMachine"></div>
                        </td>
                    <?php endif ?>

                    </br>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosMachine)):?>
                        </br>
                    <?php endif ?>


                    <input type="submit" value="Show" class="button" onclick="getBalanceInfo(); return false" />
                </form>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        </br>
        <div id="divBalance"></div>


    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>

    $( document ).ready(function() {

        var nivelUsuario = <?php echo $_SESSION['NivelUsuario']?>

        if(nivelUsuario == 1){
            getAgentGroup(<?php echo $userID?>);
        }else if(nivelUsuario == 2){
            getStoreGroup(<?php echo $userID?>);
        }else if(nivelUsuario == 3){
            getMachineGroup(<?php echo $userID?>);
        }


    });


    $('.datepicker').datepicker({

    });

    $('#dp3').datepicker();

    //getBalanceInfo
    function getBalanceInfo(){
        var agent = -1;
        var store = -1;
        var machine = -1;

        if($('#slcAgent').length){
            agent = $('#slcAgent').val();
        }
        if($('#slcStore').length){
            store = $('#slcStore').val();
        }
        if($('#slcMachine').length){
            machine = $('#slcMachine').val();
        }

        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val(),
            "Agent" : agent,
            "Store" : store,
            "Machine" : machine
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_balance_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divBalance").html("Loading Data...");
            },
            success: function(response){
                $("#divBalance").html(response);
            }
        });

    }//FIN getBalanceInfo

    /***********DROPDOWN METHODS********************/
    function getAgentGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Agent"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divAgent").html("</br>Loading...");
            },
            success: function(response){
                $("#divAgent").html(response);
            }
        });
    }//Fin getAgentGroup

    function getStoreGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Store"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divStore").html("</br>Loading...");
            },
            success: function(response){
                $("#divStore").html(response);
            }
        });
    }//Fin getStoreGroup

    function getMachineGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Machine"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divMachine").html("</br>Loading...");
            },
            success: function(response){
                $("#divMachine").html(response);
            }
        });
    }//Fin getMachineGroup

    /***********DROPDOWN METHODS********************/

</script>

