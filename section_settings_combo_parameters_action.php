<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 08/05/15
 * Time: 05:52 PM
 */
include("config.ini.php");
include("conectadb.php");


$action = $_POST['Action'];

try{
    if($action == 'parameter'){
        /*POST parameters*/
        $HitType = $_POST['HitType'];
        $HitValue = $_POST['HitValue'];
        $HitActive = $_POST['HitActive'];

        /*PDO Update*/
        $sqlUpdateParameter = "UPDATE Scoring_Parameters
                               SET pays = :pay
                               WHERE scoring_type = :hitType";
        $stmtUpdateParameter = $pdoConn->prepare($sqlUpdateParameter);

        for($i = 0; $i < sizeof($HitType); $i++){
            $stmtUpdateParameter->execute(array(':pay' => $HitValue[$i], ':hitType' => $HitType[$i]));
        }//FIN for

        /*PDO Update*/
        $sqlUpdateParameter = "UPDATE Scoring_Types
                               SET  active = :active
                               WHERE id = :hitType";
        $stmtUpdateParameter = $pdoConn->prepare($sqlUpdateParameter);

        for($i = 0; $i < sizeof($HitType); $i++){
            $stmtUpdateParameter->execute(array(':hitType' => $HitType[$i], ':active' => $HitActive[$i]));
        }//FIN for

        //Return message
        echo "Quatro parameters updated successfully";

    }//Fin IF
    elseif($action == 'Bet'){
        /*POST parameters*/
        $HitType = $_POST['HitType'];
        $Min = $_POST['Min'];
        $Max = $_POST['Max'];

        /*PDO Update*/
        $sqlUpdateBet = "UPDATE Scoring_Limits
                         SET mini = ?, maxi = ?
                         WHERE scoring_type = ?";
        $stmtUpdateBet = $pdoConn->prepare($sqlUpdateBet);

        for($i = 0; $i < sizeof($Min); $i++){
            $stmtUpdateBet->execute(array($Min[$i], $Max[$i], $HitType[$i]));
        }//FIN for

        //Return message
        echo "Bet parameters updated successfully";
    }//Fin if
    elseif($action == 'Ticket'){
        /*POST parameters*/
        $Min = $_POST['Min'];
        $Max = $_POST['Max'];

        /*PDO Update*/
        $sqlUpdateBet = "UPDATE Ticket_Parameters
                         SET mini = ?, maxi = ?
                         WHERE id = 1";
        $stmtUpdateBet = $pdoConn->prepare($sqlUpdateBet);
        $stmtUpdateBet->execute(array($Min, $Max));

        //Return message
        echo "Ticket parameters updated successfully";
    }

}catch (Exception $e){
    echo('ERROR');
}//FIN TRY/CATCH