<?php

$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

$dateFrom = date('m/d/Y', strtotime($dateTo . ' - 7 days'));
$userID = $_SESSION['IDUsuario'];

?>



<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Collections</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <form>
                    <label>FROM</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label>TO</label> <input type="text" value="<?php echo $dateTo ?>" id="toDate" class="datepicker">

                    <input type="submit" value="Show" class="button" onclick="getCollectionInfo(); return false" />
                </form>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        </br>
        <div id="divCollection"></div>


    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>


    $('.datepicker').datepicker({

    });

    $('#dp3').datepicker();

    //getBalanceInfo
    function getCollectionInfo(){

        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val()
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_collections_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divCollection").html("Loading Data...");
            },
            success: function(response){
                $("#divCollection").html(response);
            }
        });

    }//FIN getBalanceInfo

    /***********DROPDOWN METHODS********************/
    function getAgentGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Agent"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divAgent").html("</br>Loading...");
            },
            success: function(response){
                $("#divAgent").html(response);
            }
        });
    }//Fin getAgentGroup

    function getStoreGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Store"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divStore").html("</br>Loading...");
            },
            success: function(response){
                $("#divStore").html(response);
            }
        });
    }//Fin getStoreGroup

    function getMachineGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Machine"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divMachine").html("</br>Loading...");
            },
            success: function(response){
                $("#divMachine").html(response);
            }
        });
    }//Fin getMachineGroup

    /***********DROPDOWN METHODS********************/

</script>