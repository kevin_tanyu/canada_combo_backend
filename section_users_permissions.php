<?php

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$sqlGetUsuarios = "SELECT U.ID, U.NombreUsuario, UN.jerarquia FROM Usuarios U JOIN UsuariosNivel UN ON U.NivelUsuario = UN.id
                   WHERE NivelUsuario > 1 AND NivelUsuario < 4";
$stmtGetUsuarios = $pdoConn->prepare($sqlGetUsuarios);
$stmtGetUsuarios->execute();
$Usuarios = $stmtGetUsuarios->fetchAll(PDO::FETCH_ASSOC);

$sqlGetUsersActions = "SELECT M.MenuNombre, MA.id, MA.Accion
                       FROM Menus M JOIN Menus_Accion MA ON M.id = MA.idMenu";
$stmtGetUsersActions = $pdoConn->prepare($sqlGetUsersActions);
$stmtGetUsersActions->execute();
$actions = $stmtGetUsersActions->fetchAll(PDO::FETCH_ASSOC);

?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Permissions Control Panel</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Permissions
                    </div>
                    <!-- .panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-bordered " id="dataTables-actions">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($Usuarios as $user):?>
                                    <tr>
                                        <td><?php echo $user['ID']?></td>
                                        <td><?php echo $user['NombreUsuario']?></td>
                                        <td><?php echo $user['jerarquia']?></td>
                                    </tr>
                                <?php endforeach?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

            <div id="divUserAction"></div>
        </div>



        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>
    $(document).ready(function() {
        $('#dataTables-actions').DataTable({
            responsive: true
        });
        $('#dataTables-users').DataTable({
            responsive: true
        });

        var table = $('#dataTables-actions').DataTable();

        $('#dataTables-actions tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                //alert($(this).text().split("\n")[1].trim());
                var userID = $(this).text().split("\n")[1].trim();
                var parametros = {
                    "userID" : userID
                };
                $.ajax({
                    data : parametros,
                    url: 'section_users_permissions_display.php',
                    type: 'post',
                    beforeSend: function(){
                        $("#divUserAction").html("Loading Data...");
                    },
                    success: function(response){
                        $("#divUserAction").html(response);
                    }
                });
            }
        } );
    });
</script>