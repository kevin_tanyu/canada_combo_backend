<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$accion = $_POST['Accion'];
$nivelUsuario = $_SESSION['NivelUsuario'];

try{

    if($accion == 'Machine'){
        $userIDs = $_POST['userIDs'];
        $dateFrom = date("Y-m-d 00:01");
        $dateTo = date("Y-m-d 23:59");
        $today = date('Y-m-d');
        if($dateFrom == $today){
            $dateFrom = date('Y-m-d H:i:s');
        }
        $sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
        $stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
        $stmtGetLastSorteo->execute(array($dateFrom));
        $LastSorteo = $stmtGetLastSorteo->fetch();

        if($stmtGetLastSorteo->rowCount() > 0){
            $dateFrom = $LastSorteo['FechayHora'];
        }else{
            /*SI NO HAY SORTEO ANTES DE LA FECHA INGRESADA CAPTURA EL SIGUIENTE MAS PROXIMO*/
          /*  $sqlGetNextSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora > ?
                     ORDER BY FechayHora ASC LIMIT 1";
            $stmtGetNextSorteo = $pdoConn->prepare($sqlGetNextSorteo);
            $stmtGetNextSorteo->execute(array($dateFrom));
            $NextSorteo = $stmtGetNextSorteo->fetch();
            $dateFrom = $NextSorteo['FechayHora'];*/

        }//FIN if/else
        /*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
        if($dateTo != $today){
            $stmtGetLastSorteo->execute(array($dateTo));
            $LastSorteo = $stmtGetLastSorteo->fetch();


            if($stmtGetLastSorteo->rowCount() > 0){
                $dateTo = $LastSorteo['FechayHora'];
            }
        }
        /*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
        if($dateFrom == $dateTo){
            $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
            $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
        }

        $query = "";
        //ARMAR LA CLAUSULA IN
        for($i=0; $i< sizeof($userIDs); $i++){
            if($i + 1 < sizeof($userIDs)){
                $query = $query . $userIDs[$i] . ",";
            }else{
                $query = $query . $userIDs[$i];
            }
        }//FIn for

        $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                        FROM PAR_Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                        WHERE P.usuarioID IN (". $query .")
                        ORDER BY P.usuarioID";
    }//Fin if
    else if($accion == 'Filter'){

        /************OBTENER HIJOS POR ID DEL USUARIO****************/
        $sqlGetHijos = "SELECT *
                FROM  Usuarios
                WHERE  IDPadre = ?";

        $stmtGetHijos = $pdoConn->prepare($sqlGetHijos);
        /**************************************************/
       /* $stmtGetHijos->execute(array($userID));
        $sonUsers1 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);*/

        //CAPTURA LOS DATOS DEL POST
        $dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
        $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');

        $dateFrom = date("Y-m-d 00:01", strtotime($dateFrom));
        $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
        $today = date('Y-m-d');
        if($dateFrom == $today){
            $dateFrom = date('Y-m-d H:i:s');
        }

        $sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
        $stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
        $stmtGetLastSorteo->execute(array($dateFrom));
        $LastSorteo = $stmtGetLastSorteo->fetch();

        if($stmtGetLastSorteo->rowCount() > 0){
            $dateFrom = $LastSorteo['FechayHora'];
        }else{

            /*SI NO HAY SORTEO ANTES DE LA FECHA INGRESADA CAPTURA EL SIGUIENTE MAS PROXIMO*/
           /* $sqlGetNextSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora > ?
                     ORDER BY FechayHora ASC LIMIT 1";
            $stmtGetNextSorteo = $pdoConn->prepare($sqlGetNextSorteo);
            $stmtGetNextSorteo->execute(array($dateFrom));
            $NextSorteo = $stmtGetNextSorteo->fetch();
            $dateFrom = $NextSorteo['FechayHora'];*/

        }//FIN if/else
        /*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
        if($dateTo != $today){
            $stmtGetLastSorteo->execute(array($dateTo));
            $LastSorteo = $stmtGetLastSorteo->fetch();


            if($stmtGetLastSorteo->rowCount() > 0){
                $dateTo = $LastSorteo['FechayHora'];
            }
        }
        /*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
        if($dateFrom == $dateTo){
            $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
            $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
        }


        $agent = $_POST['Agent'];
        $store = $_POST['Store'];
        $machine = $_POST['Machine'];
        $userID = $_SESSION['IDUsuario'];

        $machineIDsArray = Array();

        if($nivelUsuario == 1){
            if($agent == 0){
                $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                                AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                ORDER BY P.usuarioID";
            }else{
                if($store == 0){
                    $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                    FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                    WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent ."  )))
                                    AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                    ORDER BY P.usuarioID";
                }else{
                   if($machine == 0){
                       $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
                   }else{
                       $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $machine ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
                   }//Machine

                }//Store

            }//Agent

        }elseif($nivelUsuario == 2){

            if($store == 0){
                $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                    FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                    WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  )))
                                    AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                    ORDER BY P.usuarioID";
            }else{
                if($machine == 0){
                    $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
                }else{
                    $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $machine ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
                }//Machine

            }//Store

        }elseif($nivelUsuario == 3){

            if($machine == 0){
                $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre = ". $userID ."  )
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
            }else{
                $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $machine ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
            }//Machine

        }elseif($nivelUsuario == 4){
            $sqlGetSales = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $userID ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";
        }




    }//Fin if

    $stmtGetSales = $pdoConn->prepare($sqlGetSales);
    $stmtGetSales->execute();
    $ticketSales = $stmtGetSales->fetchAll(PDO::FETCH_ASSOC);
    $totalSales = 0;

}catch (Exception $e){
    echo 'ERROR';
}


/*PARAM
 * $userID = Id del usuario
 * $nivelUsuario = Nivel del usuario
 * $pdoConn = Conn BD
 */



?>

<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Sales Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales1">
                    <thead>
                    <tr>
                        <th>Seller</th>
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($ticketSales as $sale):?>
                        <tr class="gradeA">
                            <td><?php echo $sale['NombreUsuario']?></td>
                            <td><?php echo system_date_format($sale['created_at'])?></td>
                            <td><?php echo system_number_money_format($sale['total'])?></td>
                            <?php $totalSales = $totalSales + $sale['total'];?>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<div class="col-lg-4">
    <div class="panel panel-info">
        <div class="panel-heading">

        </div>
        <div class="panel-body">
            <h2 style="color: green"><i class="fa fa-money"></i>    Balance: <?php echo system_number_money_format($totalSales)?></h2>
        </div>
        <div class="panel-footer">

        </div>
    </div>
</div>
<!-- /.col-lg-4 -->

<script>
    $(document).ready(function() {
        $('#dataTables-sales1').DataTable({
            responsive: true
        });
    });
</script>