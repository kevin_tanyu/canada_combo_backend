<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 03:43 PM
 */
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

$dateFrom = date('Y-m-d');
$dateFrom = date('Y-m-d', strtotime($dateFrom. ' - 7 days'));
$nowDate = date('Y-m-d H:i:s');

/*OBTENER LOS SORTEOS FINALIZADOS QUE NO TENGAN NUMERO GANADOR*/
$sqlSorteosSinGanador = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora, SP.IDSorteoDefinicion
                         FROM SorteosProgramacion SP
                         JOIN SorteosDefinicion SD
                         ON SP.IDSorteoDefinicion = SD.ID
                         WHERE NOT EXISTS(SELECT * FROM SorteosNumerosGanadores SG WHERE SG.IDSorteoProgramacion = SP.ID)
			             AND SP.FechayHora BETWEEN ? AND ?
                         ORDER BY SP.FechayHora Desc LIMIT 10";
$stmtSorteosSinGanador = $pdoConn->prepare($sqlSorteosSinGanador);
$stmtSorteosSinGanador->execute(array($dateFrom, $nowDate));
$sorteosSinGanador = $stmtSorteosSinGanador->fetchAll(PDO::FETCH_ASSOC);


/*OBTENER LOS NUMEROS GANADORES DE CADA SORTEO*/
$sqlSorteosConGanador = "SELECT SP.ID as sorteoID, SP.IDSorteoDefinicion, SD.NombreSorteo, SP.FechayHora, SG.ID
                         FROM SorteosProgramacion SP
                         JOIN SorteosDefinicion SD
                         ON SP.IDSorteoDefinicion = SD.ID
			             JOIN SorteosNumerosGanadores SG
                         ON SG.IDSorteoProgramacion = SP.ID
                         WHERE SP.ID = SG.IDSorteoProgramacion
                         AND SP.FechayHora BETWEEN ? AND ?
                         ORDER BY SP.FechayHora Desc LIMIT 10";
$stmtSorteosConGanador = $pdoConn->prepare($sqlSorteosConGanador);
$stmtSorteosConGanador->execute(array($dateFrom, $nowDate));
$sorteosConGanador = $stmtSorteosConGanador->fetchAll(PDO::FETCH_ASSOC);

$sqlSorteoGanadorPart = "SELECT * FROM SorteosNumerosGanadores_Part WHERE IDSorteoNumeroGanador = ?";
$stmtSorteoGanadorPart = $pdoConn->prepare($sqlSorteoGanadorPart);


$contadorEdicion = 0;
?>



        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Waiting Winner Number
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Draw</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($sorteosSinGanador as $sorteo):?>
                                    <tr>
                                        <th> <?php echo $sorteo['NombreSorteo'] ?></th>
                                        <th><?php echo system_date_format($sorteo['FechayHora']) ?></th>
                                        <th><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalS<?php echo $sorteo['ID']?>">Add numbers</button></th>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModalS<?php echo $sorteo['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" id="myModalLabel">Add Winner Number</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p class="text-info" style="font-size: 16px">Add winner number for <?php echo $sorteo['NombreSorteo']?> Draw. <?php echo system_date_format($sorteo['FechayHora']) ?> </p>
                                                        <p>
                                                        <p class="text-primary" style="font-size: 17px">1st Number</p>
                                                        <input id="txtNumeroGanador<?php echo $sorteo['ID']?>1" class="form-control" onkeypress='return isNumber(event, "txtNumeroGanador", <?php echo $sorteo['ID']?>, 2);' onkeyup='limit(this)' style="width: 100px" autofocus>
                                                        </p>
                                                        <p class="text-primary" style="font-size: 17px">2nd Number</p>
                                                        <input id="txtNumeroGanador<?php echo $sorteo['ID']?>2" class="form-control" onkeypress='return isNumber(event, "txtNumeroGanador", <?php echo $sorteo['ID']?>, 3);' onkeyup='limit(this)' style="width: 100px" >
                                                        </p>
                                                        <p class="text-primary" style="font-size: 17px">3rd Number</p>
                                                        <input id="txtNumeroGanador<?php echo $sorteo['ID']?>3" class="form-control" onkeypress='return isNumber(event, "txtNumeroGanador", <?php echo $sorteo['ID']?>, 4);' onkeyup='limit(this)' style="width: 100px" >
                                                        </p>
                                                        <p class="text-primary" style="font-size: 17px">4th Number</p>
                                                        <input id="txtNumeroGanador<?php echo $sorteo['ID']?>4" class="form-control" onkeypress='return isNumber(event, "txtNumeroGanador", <?php echo $sorteo['ID']?>, 5);' onkeyup='limit(this)' style="width: 100px" >
                                                        </p>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button id="btnAdd<?php echo $sorteo['ID']?>" type="button" class="btn btn-primary" onclick="addNumeroGanador(<?php echo $sorteo['ID']?>, <?php echo $sorteo['IDSorteoDefinicion']?>)">Add</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Finished Draw
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Draw</th>
                                    <th>Date</th>
                                    <th>Winner Numbers</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <?php foreach($sorteosConGanador as $sorteo):?>
                                    <?php $stmtSorteoGanadorPart->execute(array($sorteo['ID']));
                                          $sorteoPart = $stmtSorteoGanadorPart->fetchAll(PDO::FETCH_ASSOC);?>
                                    <th> <?php echo $sorteo['NombreSorteo'] ?></th>
                                    <th><?php echo system_date_format($sorteo['FechayHora']) ?></th>
                                    <th><?php echo $sorteoPart[0]['Numero'] . " - " . $sorteoPart[1]['Numero'] . " - " . $sorteoPart[2]['Numero'] . " - " . $sorteoPart[3]['Numero'] ?></th>
                                    <th><button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalC<?php echo $sorteo['ID']?>">Edit</button></th>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModalC<?php echo $sorteo['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit Winner Number</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-info" style="font-size: 16px">Edit winner number for <?php echo $sorteo['NombreSorteo']?> Draw. <?php echo system_date_format($sorteo['FechayHora']) ?> </p>
                                                    <p class="text-primary" style="font-size: 17px">1st Number</p>
                                                    <input id="txtEditNumeroGanador<?php echo $sorteo['ID']?>1" class="form-control" onkeypress='return isNumber(event, "txtEditNumeroGanador", <?php echo $sorteo['ID']?>, 2);' onkeyup='limit(this)' style="width: 100px" value="<?php echo $sorteoPart[0]['Numero']?>" autofocus>
                                                    </p>
                                                    <p class="text-primary" style="font-size: 17px">2nd Number</p>
                                                    <input id="txtEditNumeroGanador<?php echo $sorteo['ID']?>2" class="form-control" onkeypress='return isNumber(event, "txtEditNumeroGanador", <?php echo $sorteo['ID']?>, 3);' onkeyup='limit(this)' style="width: 100px" value="<?php echo $sorteoPart[1]['Numero']?>">
                                                    </p>
                                                    <p class="text-primary" style="font-size: 17px">3rd Number</p>
                                                    <input id="txtEditNumeroGanador<?php echo $sorteo['ID']?>3" class="form-control" onkeypress='return isNumber(event, "txtEditNumeroGanador", <?php echo $sorteo['ID']?>, 4);' onkeyup='limit(this)' style="width: 100px" value="<?php echo $sorteoPart[2]['Numero']?>">
                                                    </p>
                                                    <p class="text-primary" style="font-size: 17px">4th Number</p>
                                                    <input id="txtEditNumeroGanador<?php echo $sorteo['ID']?>4" class="form-control" onkeypress='return isNumber(event, "txtEditNumeroGanador", <?php echo $sorteo['ID']?>, 5);' onkeyup='limit(this)' style="width: 100px" value="<?php echo $sorteoPart[3]['Numero']?>">
                                                    </p>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button id="btnEdit<?php echo $sorteo['ID']?>" type="button" class="btn btn-primary" onclick="updateNumeroGanador(<?php echo $sorteo['ID']?>, <?php echo $sorteo['sorteoID']?>)">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

        </div>
        <!-- /.row -->






<script>

    //Limita que no pueda ingresar mas de 2 numeros
    function limit(element)
    {
        var max_chars = 1;

        if(element.value.length > max_chars) {
            element.value = element.value.substr(0, max_chars);
        }
    }

    //Verifica si es numero o no
    function isNumber(evt, name, sorteoID, pos) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        //if(charCode == 13){
            if(pos < 5){
                $("#"+ name + sorteoID + pos).focus();
            }
        //}
        else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }//FIN isNumber

    //addNumeroGanador
    function addNumeroGanador(idSorteo, sorteoDefinicion){

        if($('#txtNumeroGanador' + idSorteo + 1).val() != '' && $('#txtNumeroGanador' + idSorteo + 2).val() != '' && $('#txtNumeroGanador' + idSorteo + 3).val() != '' && $('#txtNumeroGanador' + idSorteo + 4).val() != ''){
            if (confirm("Are you sure to add those numbers?"))
            {
                $("#btnAdd" + idSorteo).attr("data-dismiss", "modal");
                var numerosGanadores = new Array();
                numerosGanadores.push($('#txtNumeroGanador' + idSorteo + 1).val());
                numerosGanadores.push($('#txtNumeroGanador' + idSorteo + 2).val());
                numerosGanadores.push($('#txtNumeroGanador' + idSorteo + 3).val());
                numerosGanadores.push($('#txtNumeroGanador' + idSorteo + 4).val());
                var parametros = {
                    "SorteoID" : idSorteo,
                    "NumeroGanador" : numerosGanadores,
                    "SorteoDefinicion" : sorteoDefinicion,
                    "Accion" : 'Add'
                };
                $.ajax({
                    data: parametros,
                    url: 'section_settings_winning_number_data.php',
                    type: 'post',
                    beforeSend: function(){
                        $("#divAddMsg").html("Please.. Wait a moment");
                    },
                    success: function(response){
                        if(response == 'ERROR'){
                            alert("A error has ocurred. Please try again");

                        }else{
                            alert(response);
                        }
                        //RECARGA EL DIV DE WINNINGNUMBER.PHP

                    //    $("#dataSorteos").load("section_settings_winning_number.php");
                        window.location.replace("section_settings_winning_number.php");

                    }
                });
            }//Fin fi confirm
        }else{
            alert("Fill empty(s) spaces");
        }

    }//FIN addNumeroGanador

    //addNumeroGanador
    function updateNumeroGanador(idSorteoNumeroGanador, sorteoID){

        if($('#txtEditNumeroGanador' + idSorteoNumeroGanador + 1).val() != '' && $('#txtEditNumeroGanador' + idSorteoNumeroGanador + 2).val() != '' && $('#txtEditNumeroGanador' + idSorteoNumeroGanador + 3).val() != '' && $('#txtEditNumeroGanador' + idSorteoNumeroGanador + 4).val() != ''){
            if (confirm("Are you sure to edit the numbers?"))
            {
            $("#btnEdit" + idSorteoNumeroGanador).attr("data-dismiss", "modal");
                var numerosGanadores = new Array();
                numerosGanadores.push($('#txtEditNumeroGanador' + idSorteoNumeroGanador + 1).val());
                numerosGanadores.push($('#txtEditNumeroGanador' + idSorteoNumeroGanador + 2).val());
                numerosGanadores.push($('#txtEditNumeroGanador' + idSorteoNumeroGanador + 3).val());
                numerosGanadores.push($('#txtEditNumeroGanador' + idSorteoNumeroGanador + 4).val());
            var parametros = {
                "SorteoID" : sorteoID,
                "SorteoGanadorID" : idSorteoNumeroGanador,
                "NumeroGanador" : numerosGanadores,
                "Accion" : 'Update'
            };
            $.ajax({
                data: parametros,
                url: 'section_settings_winning_number_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#divAddMsg").html("Please.. Wait a moment");
                },
                success: function(response){
                    if(response == 'ERROR'){
                        alert("A error has ocurred. Please try again");

                    }else{
                        alert(response);
                    }
                    //RECARGA EL DIV DE WINNINGNUMBER.PHP

                   // $("#dataSorteos").load("section_settings_winning_number.php");
                    window.location.replace("section_settings_winning_number.php");

                }
            });
            }
        }else{
            alert("Fill empty(s) spaces");
        }


    }//FIN addNumeroGanador

</script>