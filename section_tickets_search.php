<?php
$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);
$usuariosMachine = array(4);

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

/*ACIERTOS*/
$sqlBetHits = "SELECT * FROM Scoring_Types WHERE active = 1";
$stmtBetHits = $pdoConn->prepare($sqlBetHits);
$stmtBetHits->execute();
$hits = $stmtBetHits->fetchAll(PDO::FETCH_ASSOC);

/*PARAMETROS*/
$sqlBetParameters = "SELECT * FROM Scoring_Parameters
                     WHERE scoring_type = ?";
$stmtBetParameters = $pdoConn->prepare($sqlBetParameters);



?>


<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Search Ticket</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6">
                <p>
                <form>

                    <label>Ticket Number</label>
                    <input class="form-control" placeholder="#" style="width: 300px" onkeypress='return isNumber(event);' id="txtTicketID" >
                    <button type="button" id="btnSearch" class="btn btn-primary" style="margin-left: 230px; margin-top: 5px" onclick="getTicketInfo()">Search</button>

                </form>



                    </p>

            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Current Payment
                    </div>
                    <div class="panel-body">
                        <p>
                        <table>
                                <td style="width: 250px">
                                <?php foreach($hits as $hit):?>
                                    <?php $stmtBetParameters->execute(array($hit['id']));
                                    $parametro = $stmtBetParameters->fetch();
                                    ?>
                                    <?php echo $hit['display_name'] . ": x" . $parametro['pays'] ?>
                                    <br>
                                <?php endforeach ?>
                                </td>

                        </table>
                        </p>
                    </div>

                </div>
            </div>
            <!-- /.col-lg-4 -->


        </div>
        <div class="row">
            <div class="col-lg-12">
                <br>
                <div id="divTicketInfo">
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->


<script>

    $(function() {
        $("form input").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('#btnSearch').click();
                return false;
            } else {
                return true;
            }
        });
    });


    //Verifica si es numero o no
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }//FIN isNumber

    function getTicketInfo(){

        var parametros = {
            "TicketID" : $('#txtTicketID').val()
        };
        $.ajax({
            data : parametros,
            url: 'section_tickets_search_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }

</script>