<?php
/**
 * Funciones generales del sistema
 * User: jorge
 * Date: 10/2/14
 * Time: 4:29 PM
 */



/**
 * @param string $date fecha a ser formateada
 * @param bool $withTime indica si se quiere la hora en el string final, default a true
 * @return string la fecha con formato
 */
function system_date_format($date, $withTime = true)
{

    $days    = array("Sun" => "Sunday", "Mon" => "Monday", "Tue" => "Tuesday", "Wed" => "Wednesday", "Thu" => "Thursday", "Fri" => "Friday", "Sat" => "Saturday");
    $months  = array("Jan" => "January", "Feb" => "February", "Mar" => "March", "Apr" => "April", "May" => "May", "Jun" => "June"
        , "Jul" => "July", "Aug" => "August", "Sep" => "September", "Oct" => "October", "Nov" => "November", "Dec" => "December");


    $time    = strtotime($date);
    $dayStr  = $days[date("D", $time)];
    $dayNum  = date("d", $time);
    $month   = $months[date("M", $time)];
    $year    = date("Y", $time);
    $timeStr = "";
    if ($withTime)
    {

       $timeStr = " at " . date("g:ia", $time);

    }

        $formatted = "$dayStr, $month $dayNum, $year" . $timeStr;


    return $formatted;
}

/**
 * @param int|float $number
 * @param int $decimals to show in number
 * @return string with number formated acording to system parameters
 */
function system_number_format($number, $decimals = 0)
{
    $return = number_format($number, $decimals, ',', '.');

    return $return;
}

/**
 * @param int $number
 * @param boolean $forDownload indicates if add the "colon" sign
 * @return string with number formated acording to system parameters
 */
function system_number_money_format($number, $forDownload = false)
{
    if (!$forDownload)
        $return = '$' . number_format($number, 0, ',', '.');
    else
        $return = number_format($number, 0, '', '');

    return $return;
}

/**
 * @param array $elements array to order, with insert order algorithm
 * @param int|string inner array key for order array
 * @param int|string number array key
 * @return array ordered array
 */
function order_numbers($elements, $compValueArrayKey)
{
    $tmpArrObj = new ArrayObject($elements);
    $a         = $tmpArrObj->getArrayCopy();
    $length    = count($a);
    for ($i = 1; $i < $length; ++$i)
    {
        $x = $a[$i];
        $j = $i;
        while ($j > 0 && $a[$j - 1][$compValueArrayKey] < $x[$compValueArrayKey])
        {
            $a[$j] = $a[$j - 1];
            $j     = $j - 1;
        }
        $a[$j] = $x;
    }

    return $a;
}

/**
 * @param array $orderedNumbers order for calculate info
 * @param int $prize percentage of premio to calculate
 * @param int $comision percentage of comision for list
 * @param int $totalList total of list
 * @return array info of cut
 */
function calculate_best_cut($orderedNumbers, $prize, $comision, $totalList)
{
    $params               = array();
    $params['prize']      = $prize;
    $params['comision']   = $comision;
    $params['total_list'] = $totalList;
    $continue             = true;
    $maxCutResult = array('owned' => 0);
    for ($i = 99; $i >= 0 && $continue; --$i)
    {
        $restTemp = calculate_cut($orderedNumbers, $i, $params);
        if ($restTemp['owned'] > $maxCutResult['owned'])
        {
            $maxCutResult = $restTemp;
        } else
        {
            $continue = false;
        }
    }

    return $maxCutResult;
}

function calculate_cut($numbers, $position, $params)
{
    $value         = $numbers[$position]['monto'];
    $cutTotal = $value * ($position + 1) + sum_rest($numbers, $position);
    $comisionValue = $cutTotal * $params['comision'];
    $payValue      = $value * $params['prize'];
    $owned = $cutTotal - $comisionValue - $payValue;

    $info              = array();
    $info['pos']       = $position + 1;
    $info['cut']       = $value;
    $info['cut_total'] = $cutTotal;
    $info['comision']  = $comisionValue;
    $info['pay_perc']  = $params['prize'];
    $info['pay_value'] = $payValue;
    $info['owned']     = $owned;

    return $info;
}

function sum_rest($numbers, $position)
{
    $rest = 0;
    for ($i = $position + 1; $i < 100; ++$i)
    {
        $rest += $numbers[$i]['monto'];
    }

    return $rest;
}

function make_cut($numeros, $cut)
{
    $totalCut = 0;
    $total = 0;
    foreach ($numeros as &$val)
    {
        $difference       = $val['monto'] - $cut;
        $montoRecortado = $cut;
        if ($difference < 0)
        {
            $totalCut += $val['monto'];
            $montoRecortado = $val['monto'];
            $val['monto'] = 0;
        } else
        {
            $val['monto'] = $difference;
            $totalCut += $cut;
        }
        $total += $val['monto'];
        $val['monto_de_recorte'] = $montoRecortado;
    }

    return array('numeros' => $numeros, 'total_cut' => $totalCut, 'total_new_list' => $total);
}

/**
 * source: http://php.net/manual/en/function.ceil.php#85430
 * @param $number
 * @param int $significance
 * @return bool|float
 */
function system_ceil($number, $significance = 1)
{
    return (is_numeric($number) && is_numeric($significance)) ? (ceil($number / $significance) * $significance) : false;
}

//limpia el valor de una columna de una lista
//produce una lista nueva como copia de la que entra por parametro
function clean_list_values($list, $columConf)
{
    $tmpArrObj = new ArrayObject($list);
    $listCopy  = $tmpArrObj->getArrayCopy();
    foreach ($listCopy as &$val)
    {
        foreach ($columConf as $columnKey => $cleanValue)
            $val[$columnKey] = $cleanValue;
    }

    return $listCopy;
}

/**
 * Calcula los montos de la lista para los numeros que sobrepasen el porcentaje,
 * y retorna sus valores en un arrray con el resultado completo, y los totales de maximo, excedente
 * y la recompra
 */
function calculo_lista_general($numerosOriginal, $numeros, $totalLista, $porcentaje = 2.75)
{
    $excedenteTotal      = 0;
    $excedenteTotalFinal = 0;
    $montoMaximoFinal    = 0;
    $recompraTotal = 0;
    for ($i = 0; $i < 5; ++$i)
    {
        $totalLista     = $totalLista - $excedenteTotal;
        $excedenteTotal = 0; //una vez que se usa el excedente se tiene que limpiar
        $maxPermitido   = ($totalLista / 100) * $porcentaje;
        foreach ($numeros as &$numeroInfo)
        {
            $numeroMonto = $numeroInfo['monto'];
            if ($numeroMonto > $maxPermitido)
            {
                $excedente               = $numeroMonto - $maxPermitido;
                $numeroInfo['monto']     = $numeroMonto - $excedente;
                $numeroInfo['excedente'] = $numeroInfo['excedente'] + $excedente;
                $excedenteTotal          = $excedenteTotal + $excedente;
            }
        }
    }

    $resultadoFinal = array();

    foreach ($numerosOriginal as $key => $numInf)
    {
        //obtencion de datos finales
        $lMontoNumero     = $numInf['monto'];
        $lNumeroExcedente = $numeros[$key]['excedente'];
        $lMontoMaximo     = $lMontoNumero - $lNumeroExcedente;

        //redondeo
        $lNumeroExcedente = system_ceil($lNumeroExcedente, 10);
        $lMontoMaximo     = system_ceil($lMontoMaximo, 10);

        //informacion extra
        $redondearAl = 25;
        $lRecompra   = system_ceil($lNumeroExcedente + (($lNumeroExcedente / 1000) * 133), $redondearAl);
        $recompraTotal += $lRecompra;

        $resultadoFinal[$numInf['Numero']] = array(
            'Numero' => $numInf['Numero'],
            'monto' => $lMontoNumero,
            'monto_maximo' => $lMontoMaximo,
            'excedente' => $lNumeroExcedente,
            'recompra' => $lRecompra,
        );

        $excedenteTotalFinal += $lNumeroExcedente;
        $montoMaximoFinal += $lMontoMaximo;
    }

    return array(
        'result' => $resultadoFinal,
        'excedenteTotalFinal' => $excedenteTotalFinal,
        'montoMaximoFinal' => $montoMaximoFinal,
        'recompraTotal' => $recompraTotal
    );

}

//para el segundo proceso
function make_se_deja($numbers, $factorDeja, $montoKey)
{
    $totalLista       = 0;
    $totalListaSeDeja = 0;
    foreach ($numbers as &$number)
    {
        $number['bef2_monto']          = $number[$montoKey];
        $seDeja                        = $number[$montoKey] * $factorDeja;
        $sobrante                      = $number[$montoKey] - $seDeja;
        $number['monto'] = $sobrante + $number['excedente'];
        $number['monto_lista_se_deja'] = $seDeja;
        $totalListaSeDeja += $seDeja;
        $totalLista += $number['monto'];
    }

    return array('numeros' => $numbers, 'total_lista' => $totalLista, 'total_lista_deja' => $totalListaSeDeja);
}

function calculate_excedente_final_list($list, $montoColumn)
{
    $newListTemp = new ArrayObject($list);
    $newList     = $newListTemp->getArrayCopy();
    $total       = 0;
    foreach ($newList as &$value)
    {
        $val = $value[$montoColumn] + $value[$montoColumn] / 1000 * 133;
        $value['excedente_final'] = $val;
        $total += $val;
    }

    return array('list' => $newList, 'total' => $total);
}


function genera_tabla_tres($list, $columNum, $columValue)
{
    $finalTotal  = 0;
    $tableString = "<html>";
    $tableString .= "<table>";
    $indexLimits = array(
        array('index' => 0, 'limit' => 32, 'total' => 0),
        array('index' => 33, 'limit' => 65, 'total' => 0),
        array('index' => 66, 'limit' => 99, 'total' => 0),
    );
    $continue    = true;
    while ($continue)
    {
        $tableString .= "<tr>";
        $innerContinue = 0;
        foreach ($indexLimits as $key => $indexLimit)
        {
            $result = get_index_limit_values($indexLimit['index'], $indexLimit['limit'], $list, $columNum, $columValue);
            $tableString .= "<td style='background-color: yellow;' > " . $result['numero'] . " </td>";
            $tValue = $result['value'] ? system_number_money_format($result['value'], true) : '';
            $tableString .= "<td> " . $tValue . " </td>";
            $tableString .= "<td></td>";
            $indexLimits[$key]['total'] += ($result['value'] ? : 0);
            $finalTotal += ($result['value'] ? : 0);
            ++$indexLimits[$key]['index'];
            $innerContinue += $result['continue'];
        }
        $tableString .= "</tr>";
        if ($innerContinue == 0)
            $continue = false;
    }

    //totales
    $tableString .= "<tr>";
    foreach ($indexLimits as $indexLimit)
    {
        $tableString .= "<td style='background-color: yellow;'></td>";
        $tableString .= "<td> " . system_number_money_format($indexLimit['total'], true) . " </td>";
        $tableString .= "<td></td>";
    }
    $tableString .= "</tr>";

    //total final
    $tableString .= "<tr><td colspan='6' style='text-align: center;' >" . system_number_money_format($finalTotal, true) . "</td></tr>";
    $tableString .= "</table></html>";
    return $tableString;

}

function get_index_limit_values($index, $limit, $list, $numberKey, $valueKey)
{
    $result = array('numero' => null, 'value' => null, 'continue' => 1);
    if ($index <= $limit)
    {
        $result['numero'] = $list[$index][$numberKey];
        $result['value']  = $list[$index][$valueKey];
    } else
    {
        $result['continue'] = 0;
    }

    return $result;
}

//sql para lista general
//esto porque el query se usa en dos scripts distintos
function getGeneralDetalleSql()
{
    $sql = "SELECT
              N.Numero,
               SUM( IFNULL(CSA.Cantidad,0) + IFNULL(SA.Cantidad,0) ) AS monto
              ,0 as excedente
            FROM Numeros N
              LEFT JOIN ( SELECT CSAV.Numero, SUM(CSAV.Cantidad) AS Cantidad
                          FROM CLIENTE_SorteoApuesta_Validas CSAV
                          JOIN Usuarios_View P ON P.ID = CSAV.IDUsuario
                          WHERE IDSorteoProgramacion = :sorteo_programacion_id
                            AND P.IDPadre IN (SELECT ID FROM Usuarios_View WHERE en_lista_general = 1)
                          GROUP BY Numero
                        )CSA ON CSA.Numero = N.numero
              LEFT JOIN ( SELECT INSAV.Numero, SUM(INSAV.Cantidad) AS Cantidad
                          FROM SorteoApuesta_Validas INSAV
                          WHERE INSAV.IDSorteoProgramacion = :sorteo_programacion_id
                            AND INSAV.IDUsuario IN (SELECT IIUV.ID FROM Usuarios_View IIUV WHERE en_lista_general = 1)
                          GROUP BY INSAV.Numero
                        ) SA ON SA.Numero = N.Numero
            GROUP BY N.Numero
            WITH ROLLUP";

    return $sql;
}