<?php

session_start();

include("config.ini.php");
include("conectadb.php");

$userID = $_POST['userID'];

$sqlGetActions = "SELECT M.MenuNombre, MA.id, MA.Accion
                  FROM Menus M JOIN Menus_Accion MA ON M.id = MA.idMenu";
$stmtGetActions = $pdoConn->prepare($sqlGetActions);
$stmtGetActions->execute();
$actions = $stmtGetActions->fetchAll(PDO::FETCH_ASSOC);

$actionsIDs = array();

foreach($actions as $action){
    array_push($actionsIDs, $action['Accion']);
}

$sqlGetUserAction = "SELECT * FROM Menus_Accion_Usuarios
                     WHERE idAccion = ? AND usuarioID = ?";
$stmtGetUserAction = $pdoConn->prepare($sqlGetUserAction);


?>

<!-- /.col-lg-6 -->
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Actions
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Action</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($actions as $action):?>
                    <tr>
                        <td><?php echo $action['Accion']?> </td>
                        <?php $stmtGetUserAction->execute(array($action['id'], $userID));
                              $accionUsuario = $stmtGetUserAction->fetch();?>
                        <td><input type="radio" name="radioPermission<?php echo $action['id']?>" id="radioPermission<?php echo $action['id']?>" value="1"
                                <?php echo ($accionUsuario['activo'] == 1) ? "checked" : ""; ?>>
                            <label for="radioPermission<?php echo $action['id']?>">Yes</label>

                            <input type="radio" name="radioPermission<?php echo $action['id']?>" id="radioPermission<?php echo $action['id']?>" value="0"
                                <?php echo ($accionUsuario['activo'] == null || $accionUsuario['activo'] == 0) ? "checked" : "";?>>
                            <label for="radioPermission<?php echo $action['id']?>">No</label></td>
                    </tr>
                    <?php endforeach ?>
                    <tr>
                        <td><div id="divUpdateAction"></div></td>
                        <td><button type="button" class="btn btn-default" onclick="savePermissions()">Save</button></td>
                    </tr>
                    <tr></tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->

<script>

    function savePermissions(){

        var actionsIDs = new Array();
        <?php foreach($actions as $action):?>
                 actionsIDs.push(<?php echo $action['id']?>);
        <?php endforeach?>;

        var permissionsUser = new Array();
        for(var i = 0; i < actionsIDs.length ; i++){
            permissionsUser.push($('input[name="radioPermission'+ actionsIDs[i] +'"]:checked').val());
        }

        var parametros = {
            "permissionUser" : permissionsUser,
            "userID" : <?php echo $userID?>
        };
        $.ajax({
            data : parametros,
            url: 'section_users_permissions_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divUpdateAction").html("Please wait a moment...");
            },
            success: function(response){
                $("#divUpdateAction").html(response);
            }
        });

    }//Fin savePermissions

</script>