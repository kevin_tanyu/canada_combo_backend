<?php

$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);
$usuariosMachine = array(4);

include("config.ini.php");
include("conectadb.php");

if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

$sqlGetUserAction = "SELECT * FROM Menus_Accion_Usuarios
                     WHERE idAccion = ? AND usuarioID = ?";
$stmtGetUserAction = $pdoConn->prepare($sqlGetUserAction);

/*PERMISOS USUARIOS*/
$stmtGetUserAction->execute(array(5, $_SESSION['IDUsuario']));
$addAgent = $stmtGetUserAction->fetch();

$stmtGetUserAction->execute(array(6, $_SESSION['IDUsuario']));
$addStore = $stmtGetUserAction->fetch();

$stmtGetUserAction->execute(array(7, $_SESSION['IDUsuario']));
$addMachine = $stmtGetUserAction->fetch();

$showAdd = false;

if($addAgent['activo'] == 1 || $addStore['activo'] == 1 || $addMachine['activo'] == 1){
    $showAdd = true;
}


?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>QUATRO BACKEND</title>


    <!-- Bootstrap Core CSS -->
    <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bower_components/datepicker/css/bootstrap-datepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>

<body onload="getTime()">

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="home.php">QUATRO</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li style="margin-top: 2px">
                <label>Time: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <div id="showtime2" style="position: relative"></div></label>

            </li>
            <li style="margin-top: 2px">
                <label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Date:
                    <div id="showtime2" style="position: relative; width: 150px">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <?php echo date('m-d-Y');?></div></label>


            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li style="font-weight: bold">&nbsp&nbsp&nbsp<i class="glyphicon glyphicon-user"></i> <?php echo $_SESSION['NombreUsuario']?>
                    </li>
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <!--<li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                        <!-- /input-group
                    </li>-->
                    <li>
                        <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-file-text fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php $stmtGetUserAction->execute(array(1, $_SESSION['IDUsuario']));
                                  $showCombination = $stmtGetUserAction->fetch();?>
                            <?php if ($showCombination['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                            <li>
                                <a href="section_reports_combinations.php">Combinations</a>
                            </li>
                            <?php endif; ?>

                            <?php $stmtGetUserAction->execute(array(2, $_SESSION['IDUsuario']));
                            $showSales = $stmtGetUserAction->fetch();?>
                            <?php if ($showSales['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                            <li>
                                <a href="section_reports_sales.php">Sales</a>
                            </li>
                            <?php endif; ?>

                            <?php $stmtGetUserAction->execute(array(3, $_SESSION['IDUsuario']));
                            $showCollections = $stmtGetUserAction->fetch();?>
                            <?php if ($showCollections['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                            <li>
                                <a href="section_reports_collections.php">Collections</a>
                            </li>
                            <?php endif; ?>

                            <?php $stmtGetUserAction->execute(array(4, $_SESSION['IDUsuario']));
                            $showBalance = $stmtGetUserAction->fetch();?>
                            <?php if ($showBalance['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                            <li>
                                 <a href="section_reports_balance.php">Balance</a>
                            </li>
                            <?php endif; ?>
                            <?php $stmtGetUserAction->execute(array(8, $_SESSION['IDUsuario']));
                            $showProfit = $stmtGetUserAction->fetch();?>
                            <?php if ($showProfit['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                            <li>
                                <a href="section_reports_profit.php">Profit</a>
                            </li>
                            <?php endif; ?>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <?php $stmtGetUserAction->execute(array(8, $_SESSION['IDUsuario']));
                          $showTicket = $stmtGetUserAction->fetch();?>
                    <?php if ($showTicket['activo'] == 1 || $_SESSION['NivelUsuario'] == 1): ?>
                    <li>
                        <a href="#"><i class="fa fa-ticket fa-fw"></i> Tickets<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href=section_tickets_search.php>Search</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <?php endif; ?>

                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosHouse)): ?>
                    <li>
                        <a href="#"><i class="fa fa-calendar fa-fw"></i> Draws<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="section_settings_winning_number.php">Winner Number</a>
                            </li>
                            <li>
                                <a href="section_settings_scheduling.php">Draw Schedule</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Quatro<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="section_settings_combo_parameters.php">Parameters</a>
                                </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    <?php endif; ?>
                    <?php if ($showAdd == true || $_SESSION['NivelUsuario'] == 1): ?>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li>
                                    <a href="section_users_manage_users.php">Manage Users</a>
                                </li>
                                <?php if (in_array($_SESSION['NivelUsuario'], $usuariosHouse)): ?>
                                <li>
                                    <a href="section_users_permissions.php">Permissions</a>
                                </li>
                                <?php endif; ?>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    <?php endif; ?>



                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>



</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="./bower_components/jquery/dist/jquery.min.js"></script>

<!--[DATEPICKER]>-->
<script src="./bower_components/datepicker/js/bootstrap-datepicker.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>

<!-- DataTables JavaScript -->
<script src="./bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="./bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>


</body>

<script>

    function getTime()
    {
        var today=new Date();
        var h=today.getHours();
        var m=today.getMinutes();
        var s=today.getSeconds();
        // add a zero in front of numbers<10
        m=checkTime(m);
        s=checkTime(s);
        document.getElementById('showtime2').innerHTML=h+":"+m+":"+s;
        t=setTimeout(function(){getTime()},500);
    }

    function checkTime(i)
    {
        if (i<10)
        {
            i="0" + i;
        }
        return i;
    }

</script>


</html>
