<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

/*Captura los datos enviados*/
$sorteoID = $_POST['SorteoID'];
$numGanadores = $_POST['NumeroGanador'];
$accion = $_POST['Accion'];

try{
    if($accion == 'Add'){

        $sorteoDefinicion = $_POST['SorteoDefinicion'];
        $date = date('Y-m-d H:i:s');
        /*Query para insertar numero ganador*/
        $sqlInsertNumeroGanador = "INSERT INTO SorteosNumerosGanadores(FechayHora, ActivoFlag, IDSorteoProgramacion, IDSorteoDefinicion)
                                   VALUES(:now, 1, :sorteoID, :sorteoDefinicion)";
        $sqlInsertNumeroGanador = $pdoConn->prepare($sqlInsertNumeroGanador);
        $sqlInsertNumeroGanador->execute(array(':now' => $date, ':sorteoID' => $sorteoID, ':sorteoDefinicion' => $sorteoDefinicion));
        $numeroGanadorID = $pdoConn->lastInsertId();

        /*Query para insertar detalle numero ganador*/
        $sqlInsertNumeroGanadorDetalle = "INSERT INTO SorteosNumerosGanadores_Part(IDSorteoNumeroGanador, Orden, Numero)
                                          VALUES(:ganadorID , :orden, :numGanador)";
        $sqlInsertNumeroGanadorDetalle = $pdoConn->prepare($sqlInsertNumeroGanadorDetalle);

        for($i = 0; $i < sizeof($numGanadores); $i++){
            $sqlInsertNumeroGanadorDetalle->execute(array(':ganadorID' => $numeroGanadorID, ':numGanador' => $numGanadores[$i],':orden' => $i+1));
        }

        /*******************************SCORING**********************************/
        /*GET TIQUETES DEL SORTEO*/
        $sqlGetTicket = "SELECT * FROM Ticket WHERE sorteoID = ?";
        $stmtGetTicket = $pdoConn->prepare($sqlGetTicket);
        $stmtGetTicket->execute(array($sorteoID));
        $DrawTickets = $stmtGetTicket->fetchAll(PDO::FETCH_ASSOC);

        /*GET APUESTAS DEL TIQUETE*/
        $sqlTicketBet = "SELECT * FROM Ticket_Bet WHERE ticketID = ?";
        $stmtTicketBet = $pdoConn->prepare($sqlTicketBet);

        /*GET NUMEROS DE LA APUESTA*/
        $sqlTicketBerPart = "SELECT * FROM Ticket_Bet_Part WHERE ticketBetID = ?";
        $stmtTicketBetPart = $pdoConn->prepare($sqlTicketBerPart);

        /*SQL INSERT TIQUETE GANADOR*/
        $sqlAddWinnerTicket = "INSERT INTO Ticket_Prizes(sorteoID, ticketID, ticketBetID, scoringHitTypeID, betAmount, pays, finalPrize, createdAt)
                                          VALUES(:sorteoID, :ticketID, :ticketBetID, :scoringHitTypeID, :betAmount, :pays, :finalPrize, :createdAt)";
        $stmtAddWinnerTicket = $pdoConn->prepare($sqlAddWinnerTicket);

        /*GET CANTIDAD QUE PAGA*/
        $sqlGetPays = "SELECT pays FROM Scoring_Parameters WHERE scoring_type = ?";
        $stmtGetPays = $pdoConn->prepare($sqlGetPays);

        /*PREMIOS*/
        $sqlGetScoring = "SELECT ST.id, ST.hits_qty, SP.pays
                          FROM Scoring_Types ST JOIN Scoring_Parameters SP ON ST.id = SP.scoring_type
                          WHERE active = 1 AND ST.id = ?;";
        $stmtGetScoring = $pdoConn->prepare($sqlGetScoring);

        $contador = 0;
        foreach($DrawTickets as $ticket){

            $stmtTicketBet->execute(array($ticket['id']));
            $TicketBets = $stmtTicketBet->fetchAll(PDO::FETCH_ASSOC);

            foreach($TicketBets as $bet){

                $stmtTicketBetPart->execute(array($bet['id']));
                $ticketBetPart = $stmtTicketBetPart->fetchAll(PDO::FETCH_ASSOC);

                //PARA LLEVAR UN CONTROL DE ADONDE TERMINA LA SECUENCIA DEL SCORING
                $stillScoring = true;
                $pays = 0;//CANTIDAD DE VECES QUE SE LE PAGA EL PREMIO
                $alreadyHit = false;//PARA SABER SI PEGO ALGUNA
                $winnerScoringID = 0;//ID DEL SCORING GANADOR
                $score = false;//PARA VERIFICAR SI ESTA ACTIVO LA PAGA

                $stmtGetScoring->execute(array($bet['scoring_type']));

                if($stmtGetScoring->rowCount() > 0){
                    $score = true;
                }//Fin fin


                if($bet['scoring_type'] == 1 && $score == true){//SCOREA PICK1

                    if($numGanadores[0] == $ticketBetPart[0]['number'] ){

                        $stmtGetScoring->execute(array(1));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 1;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 2 && $score == true){//SCOREA PICK2

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number']){

                        $stmtGetScoring->execute(array(2));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 2;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 3 && $score == true){//SCOREA PICK3

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number'] && $numGanadores[2] == $ticketBetPart[2]['number']){

                        $stmtGetScoring->execute(array(3));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 3;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 4 && $score == true){//SCOREA PICK4

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number'] && $numGanadores[2] == $ticketBetPart[2]['number'] && $numGanadores[3] == $ticketBetPart[3]['number']){

                        $stmtGetScoring->execute(array(4));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 4;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 5 && $score == true){//SCOREA ANYORDER

                    //CONTADOR DE HITS
                    $hits = 0;
                    $tempGanadores = $numGanadores;
                    foreach($ticketBetPart as $betPart){

                        for($j = 0; $j < sizeof($tempGanadores); $j++){

                            if($betPart['number'] == $tempGanadores[$j]){
                                $hits++;
                                $tempGanadores[$j] = -1;
                                $j = sizeof($tempGanadores) + 1;
                            }//Fin if

                        }//Fin for
                    }//Fin foreach

                    if($hits == 4){
                        $stmtGetScoring->execute(array(5));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 5;
                        $alreadyHit = true;
                    }//Fin if hits

                }//Fin elseif


                if($alreadyHit == true){

                    $stmtGetPays->execute(array($winnerScoringID));
                    $pays = $stmtGetPays->fetch();
                    $date = date('Y-m-d H:i:s');
                    if($winnerScoringID == 5){
                        $finalPrize = $pays['pays'] * ($bet['amount']);
                    }else{
                        $finalPrize = $pays['pays'] * $bet['amount'];
                    }


                    $stmtAddWinnerTicket->execute(array(':sorteoID' => $sorteoID, ':ticketID' => $ticket['id'], ':ticketBetID' => $bet['id'], ':scoringHitTypeID' => $winnerScoringID,
                        ':betAmount' => $bet['amount'], ':pays' => $pays['pays'], ':finalPrize' => $finalPrize, ':createdAt' => $date));
                    $contador++;
                }//Fin if

            }//Fin foreach

        }//FIN foreach




        echo "Winner Numbers Successful Added";
    }else if($accion == 'Update'){

        $sorteoGanadorID = $_POST['SorteoGanadorID'];
        $sqlUpdateNumeroGanador = "UPDATE SorteosNumerosGanadores_Part
                                   SET Numero = :numGanador
                                   WHERE IDSorteoNumeroGanador = :idSorteoGanador AND Orden = :orden";
        $stmtUpdateNumeroGanador = $pdoConn->prepare($sqlUpdateNumeroGanador);
        for($i = 0; $i < sizeof($numGanadores); $i++){
            $stmtUpdateNumeroGanador->execute(array(':numGanador' => $numGanadores[$i], ':idSorteoGanador' => $sorteoGanadorID, ':orden' => $i+1));
        }

        /*******************************SCORING**********************************/
        /*BORRA LOS TIQUETES SCOREADOS PARA VOLVER A HACER EL SCORING*/
        $sqlDeletePrize = "DELETE FROM Ticket_Prizes WHERE sorteoID = ?";
        $stmtDeletePrize = $pdoConn->prepare($sqlDeletePrize);
        $stmtDeletePrize->execute(array($sorteoID));

        /*GET TIQUETES DEL SORTEO*/
        $sqlGetTicket = "SELECT * FROM Ticket WHERE sorteoID = ?";
        $stmtGetTicket = $pdoConn->prepare($sqlGetTicket);
        $stmtGetTicket->execute(array($sorteoID));
        $DrawTickets = $stmtGetTicket->fetchAll(PDO::FETCH_ASSOC);

        /*GET APUESTAS DEL TIQUETE*/
        $sqlTicketBet = "SELECT * FROM Ticket_Bet WHERE ticketID = ?";
        $stmtTicketBet = $pdoConn->prepare($sqlTicketBet);

        /*GET NUMEROS DE LA APUESTA*/
        $sqlTicketBerPart = "SELECT * FROM Ticket_Bet_Part WHERE ticketBetID = ?";
        $stmtTicketBetPart = $pdoConn->prepare($sqlTicketBerPart);

        /*SQL INSERT TIQUETE GANADOR*/
        $sqlAddWinnerTicket = "INSERT INTO Ticket_Prizes(sorteoID, ticketID, ticketBetID, scoringHitTypeID, betAmount, pays, finalPrize, createdAt)
                                          VALUES(:sorteoID, :ticketID, :ticketBetID, :scoringHitTypeID, :betAmount, :pays, :finalPrize, :createdAt)";
        $stmtAddWinnerTicket = $pdoConn->prepare($sqlAddWinnerTicket);

        /*GET CANTIDAD QUE PAGA*/
        $sqlGetPays = "SELECT pays FROM Scoring_Parameters WHERE scoring_type = ?";
        $stmtGetPays = $pdoConn->prepare($sqlGetPays);

        /*PREMIOS*/
        $sqlGetScoring = "SELECT SP.pays
                          FROM Scoring_Types ST JOIN Scoring_Parameters SP ON ST.id = SP.scoring_type
                          WHERE ST.id = ?;";
        $stmtGetScoring = $pdoConn->prepare($sqlGetScoring);

        $contador = 0;
        foreach($DrawTickets as $ticket){

            $stmtTicketBet->execute(array($ticket['id']));
            $TicketBets = $stmtTicketBet->fetchAll(PDO::FETCH_ASSOC);

            foreach($TicketBets as $bet){

                $stmtTicketBetPart->execute(array($bet['id']));
                $ticketBetPart = $stmtTicketBetPart->fetchAll(PDO::FETCH_ASSOC);

                //PARA LLEVAR UN CONTROL DE ADONDE TERMINA LA SECUENCIA DEL SCORING
                $stillScoring = true;
                $pays = 0;//CANTIDAD DE VECES QUE SE LE PAGA EL PREMIO
                $alreadyHit = false;//PARA SABER SI PEGO ALGUNA
                $winnerScoringID = 0;//ID DEL SCORING GANADOR
                $score = false;//PARA VERIFICAR SI ESTA ACTIVO LA PAGA

                $stmtGetScoring->execute(array($bet['scoring_type']));

                if($stmtGetScoring->rowCount() > 0){
                    $score = true;
                }//Fin fin

                if($bet['scoring_type'] == 1 && $score == true){//SCOREA PICK1

                    if($numGanadores[0] == $ticketBetPart[0]['number'] ){

                        $stmtGetScoring->execute(array(1));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 1;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 2 && $score == true){//SCOREA PICK2

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number']){

                        $stmtGetScoring->execute(array(2));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 2;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 3 && $score == true){//SCOREA PICK3

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number'] && $numGanadores[2] == $ticketBetPart[2]['number']){

                        $stmtGetScoring->execute(array(3));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 3;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 4 && $score == true){//SCOREA PICK4

                    if($numGanadores[0] == $ticketBetPart[0]['number'] && $numGanadores[1] == $ticketBetPart[1]['number'] && $numGanadores[2] == $ticketBetPart[2]['number'] && $numGanadores[3] == $ticketBetPart[3]['number']){

                        $stmtGetScoring->execute(array(4));
                        $prize = $stmtGetScoring->fetch();
                        $pays = $prize['pays'];
                        $winnerScoringID = 4;
                        $alreadyHit = true;

                    }//Fin if

                }elseif($bet['scoring_type'] == 5 && $score == true){//SCOREA ANYORDER

                        //CONTADOR DE HITS
                        $hits = 0;
                        $tempGanadores = $numGanadores;
                        foreach($ticketBetPart as $betPart){

                            for($j = 0; $j < sizeof($tempGanadores); $j++){

                                if($betPart['number'] == $tempGanadores[$j]){
                                    $hits++;
                                    $tempGanadores[$j] = -1;
                                    $j = sizeof($tempGanadores) + 1;
                                }//Fin if

                            }//Fin for
                        }//Fin foreach

                        if($hits == 4){
                            $stmtGetScoring->execute(array(5));
                            $prize = $stmtGetScoring->fetch();
                            $pays = $prize['pays'];
                            $winnerScoringID = 5;
                            $alreadyHit = true;
                        }//Fin if hits


                }//Fin elseif

                /* foreach($Scorings as $scoring){

                     //CONTADOR DE HITS
                     $hits = 0;
                     $tempGanadores = $numGanadores;
                     if($scoring['id'] == 5){//MIXED SE SCOREA DIFERENTE

                         for($i = 0; $i < $scoring['hits_qty']; $i++){

                             for($j = 0; $j < sizeof($tempGanadores); $j++){

                                 if($ticketBetPart[$i]['number'] == $tempGanadores[$j]){
                                     $hits++;
                                     $tempGanadores[$j] = -1;
                                 }//Fin if

                             }//Fin for
                         }//Fin for

                     }//Fin if*/
                  /*  else{
                        for($i = 0; $i < $scoring['hits_qty']; $i++){*/
                            /*COMPRAR LOS NUMEROS GANADORES CON LOS DEL TIQUETES*/
                         /*   if($numGanadores[$i] == $ticketBetPart[$i]['number']){
                                $hits++;
                            }
                        }//Fin FOR
                    }//Fin if/else*/
                    /*SI ACIERTA LA CANTIDAD EN LE TIPO DE APUESTA*/
                    /*if($hits == $scoring['hits_qty']){
                        if($scoring['pays'] > $pays){
                            $pays = $scoring['pays'];
                            $winnerScoringID = $scoring['id'];
                            $alreadyHit = true;
                        }
                    }//Fin if

                }//FIN for*/

                if($alreadyHit == true){

                    $stmtGetPays->execute(array($winnerScoringID));
                    $pays = $stmtGetPays->fetch();
                    $date = date('Y-m-d H:i:s');
                    if($winnerScoringID == 5){
                        $finalPrize = $pays['pays'] * ($bet['amount']);
                    }else{
                        $finalPrize = $pays['pays'] * $bet['amount'];
                    }

                    $stmtAddWinnerTicket->execute(array(':sorteoID' => $sorteoID, ':ticketID' => $ticket['id'], ':ticketBetID' => $bet['id'], ':scoringHitTypeID' => $winnerScoringID,
                        ':betAmount' => $bet['amount'], ':pays' => $pays['pays'], ':finalPrize' => $finalPrize, ':createdAt' => $date));
                    $contador++;
                }//Fin if

            }//Fin foreach

        }//FIN foreach


        echo "Winner Numbers Successful Updated";
    }


}catch(Excepcion $e){

    echo "ERROR";
}

?>

