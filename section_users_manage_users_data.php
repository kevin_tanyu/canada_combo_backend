<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$accion = $_POST['Accion'];


try{
    if($accion == 'Update'){

        $userID = $_POST['userID'];
        $password = $_POST['Password'];
        $email = $_POST['Email'];
        $idPadre = $_POST['Parent'];
        $Percentage = $_POST['Percentage'];

        $sqlUpdateUser = "UPDATE Usuarios
                          SET Contrasena = :password, Email = :email, IDPadre = :idPadre, Porcentaje = :porcentaje
                          WHERE ID = :userID";
        $stmtUpdateUser = $pdoConn->prepare($sqlUpdateUser);
        $stmtUpdateUser->execute(array(":password" => $password, ":email" => $email, ":idPadre" => $idPadre , ":userID" => $userID, "porcentaje" => $Percentage));

        echo "User Updated Successfully";
    }//Fin if
    elseif($accion == 'Delete'){

        $userID = $_POST['userID'];

        $sqlCheckChildren = "SELECT * FROM Usuarios WHERE IDPadre = ?";
        $stmtCheckChildren = $pdoConn->prepare($sqlCheckChildren);
        $stmtCheckChildren->execute(array($userID));

        if($stmtCheckChildren->rowCount() > 0){
            echo "User can't be deleted";
        }else{
            $sqlDeleteUser = "DELETE FROM Usuarios WHERE ID = ?";
            $stmtDeleteUser = $pdoConn->prepare($sqlDeleteUser);
            $stmtDeleteUser->execute(array($userID));

            echo "User deleted sucessfully";
        };
    }//Fin if
    elseif($accion == 'Create'){
        $username = $_POST['Username'];
        $password = $_POST['Password'];
        $email = $_POST['Email'];
        $userGroup = $_POST['UserGroup'];
        $parentID  = $_POST['ParentID'];
        $percentage = $_POST['Percentage'];


        $sqlInsertUser = "INSERT INTO Usuarios(NombreUsuario, Contrasena, NivelUsuario, ActivoFlag, IDPadre, Email, Porcentaje)
                          VALUES(:username, :password, :userGroup, 1, :parentID, :email, :porcentaje)";
        $stmtInsertUser = $pdoConn->prepare($sqlInsertUser);
        $stmtInsertUser->execute(array(':username' => $username, ':password' => $password, ':userGroup' => $userGroup, ':parentID' => $parentID, ':email' => $email, 'porcentaje' => $percentage));

        echo 'User Succesfully Added';

    }//Fin if
    elseif($accion == 'ParentGroup'){

        $parentLevel = $_POST['ParentLevel'];
        /*USUARIOS POR NIVEL*/
       $sqlUsers = "SELECT * FROM Usuarios
                     WHERE NivelUsuario = ?
                     ORDER BY IDPadre ASC";
        $stmtUsers = $pdoConn->prepare($sqlUsers);
        $stmtUsers->execute(array($parentLevel));
        $users = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);

        if($stmtUsers->rowCount() > 0){
            $response =
                  "<label>Parent</label>
                   <select class='form-control' style='width: 230px' id='slcNewParent'>";
            foreach( $users as  $user){

                $response = $response .    "<option value='" . $user['ID'] . "'>" . $user['NombreUsuario'] . "</option>";
            }

            $response = $response . "</select>";
        }else{
            $response = " ";
        }




        echo $response;

    }//Fin if

}catch (Exception $e){

    echo('ERROR');

}//FIN TRY/CATCH