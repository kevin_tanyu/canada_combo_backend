 <?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$userID = $_SESSION['IDUsuario'];
$nivelUsuario = $_SESSION['NivelUsuario'];
$ticketID = $_POST['TicketID'];
$nowTime = date("Y-m-d H:i:s");
$showPayButton = false;
$hitsQuantity = 0;
$betType = 1;

try{

    /*********INFO DEL TIQUETE************/
    if($nivelUsuario == 1){
        $sqlGetTicketInfo = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total, P.sorteoID
                             FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                             WHERE P.id = ?";
        $stmtGetTicketInfo = $pdoConn->prepare($sqlGetTicketInfo);
        $stmtGetTicketInfo->execute(array($ticketID));
    }elseif($nivelUsuario == 2){
        $sqlGetTicketInfo = "SELECT U.NombreUsuario, P.usuarioID, P.created_at, P.total, P.sorteoID
                             FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                             WHERE P.id = ? AND P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID = ?)))";
        $stmtGetTicketInfo = $pdoConn->prepare($sqlGetTicketInfo);
        $stmtGetTicketInfo->execute(array($ticketID, $userID));
    }elseif($nivelUsuario == 3){
        $sqlGetTicketInfo = "SELECT U.NombreUsuario,P.usuarioID, P.created_at, P.total, P.sorteoID
                             FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                             WHERE P.id = ? AND P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre = ?)";
        $stmtGetTicketInfo = $pdoConn->prepare($sqlGetTicketInfo);
        $stmtGetTicketInfo->execute(array($ticketID, $userID));
    }elseif($nivelUsuario == 4){
        $sqlGetTicketInfo = "SELECT U.NombreUsuario,P.usuarioID, P.created_at, P.total, P.sorteoID
                             FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                             WHERE P.id = ? AND P.usuarioID = ?";
        $stmtGetTicketInfo = $pdoConn->prepare($sqlGetTicketInfo);
        $stmtGetTicketInfo->execute(array($ticketID, $userID));
    }


    $TicketInfo = $stmtGetTicketInfo->fetch();

    if($stmtGetTicketInfo->rowCount() > 0){


        /*********SORTEOS QUE PERTENECE AL TIQUETE************/
        $sqlGetSorteo = "SELECT *
                         FROM SorteosProgramacion SP
                         JOIN SorteosDefinicion SD ON SP.IDSorteoDefinicion = SD.ID
                         WHERE SP.ID = ?";
        $stmtGetSorteo = $pdoConn->prepare($sqlGetSorteo);
        $stmtGetSorteo->execute(array($TicketInfo['sorteoID']));
        $SorteoEvento = $stmtGetSorteo->fetch();

        /*********APUESTAS CONTENIDAS EN EL TIQUETE************/
        $sqlGetBet = "SELECT *
                      FROM Ticket_Bet
                      WHERE ticketID = ? ";
        $stmtGetBet = $pdoConn->prepare($sqlGetBet);
        $stmtGetBet->execute(array($ticketID));
        $TicketBets = $stmtGetBet->fetchAll(PDO::FETCH_ASSOC);

        /*********COMBINACIONES EN LA APUESTA************/
        $sqlGetNumber = "SELECT *
                         FROM Ticket_Bet_Part
                         WHERE ticketBetID = ?";
        $stmtGetNumber = $pdoConn->prepare($sqlGetNumber);


        /*********NUMEROS GANADORES DEL SORTEO************/
        $sqlGetNumeroGanador = "SELECT Numero FROM SorteosNumerosGanadores_Part
                                WHERE IDSorteoProgramacion = ? AND Orden = ?";
        $stmtGetNumeroGanador = $pdoConn->prepare($sqlGetNumeroGanador);

        /*********CANTIDAD QUE PAGA************/
        $sqlPaymentMultiplier = "SELECT SP.pays FROM Scoring_Parameters SP
                                 JOIN Scoring_Types ST ON SP.scoring_type = ST.id
                                 WHERE hits_qty = ?";
        $stmtPaymentMultiplier = $pdoConn->prepare($sqlPaymentMultiplier);

        /***********PREMIO GANADO********************/
        $sqlPrize = "SELECT finalPrize FROM Ticket_Prizes
                     WHERE ticketID = ? AND ticketBetID = ?";
        $stmtPrize = $pdoConn->prepare($sqlPrize);

        /**********COMPROBAR SI TIQUETE FUE PAGADO*********/
        $sqlComprobarPago = "SELECT * FROM Ticket_Payment TP
                             JOIN Usuarios U ON TP.pay_by = U.ID
                             WHERE TP.ticketID = ?";
        $stmtComprobarPago = $pdoConn->prepare($sqlComprobarPago);

        $firstHit = true;

        $totalPremios = 0;

        $prizeBets = array();
        $prizeAmount = array();


    }

}catch (Exception $e){
    echo 'ERROR';
}

?>


<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ticket Info
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <?php if($stmtGetTicketInfo->rowCount() > 0){?>
                    <span style="font-size: 20px">
                        <label>Ticket #<?php echo $ticketID?></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>Draw: <?php echo system_date_format($SorteoEvento['FechayHora'])?></label></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>Seller: <?php echo $TicketInfo['NombreUsuario']?></label></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>Date: <?php echo system_date_format($TicketInfo['created_at'])?></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>Total: <?php echo system_number_money_format($TicketInfo['total'])?></label>
                    </span>
                    </br>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr align="center">
                            <th style="text-align: center">1st</th>
                            <th style="text-align: center">2nd</th>
                            <th style="text-align: center">3rd</th>
                            <th style="text-align: center">4th</th>
                            <th style="text-align: center">Amount</th>
                            <th style="text-align: center">Prize</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <?php foreach($TicketBets as $bet):?>
                                <?php $stmtGetNumber->execute(array($bet['id']));
                                     $betParts = $stmtGetNumber->fetchAll(PDO::FETCH_ASSOC);?>
                        <tr>
                            <?php if($bet['scoring_type'] == 1){ ?>
                                <td style="text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center;" ><?php echo system_number_money_format($bet['amount']); ?> </td>
                            <?php }elseif($bet['scoring_type'] == 2){ ?>
                                <td style="text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center;" ><?php echo system_number_money_format($bet['amount']); ?> </td>
                            <?php }elseif($bet['scoring_type'] == 3){ ?>
                                <td style="text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                                <td style="text-align: center" >-</td>
                                <td style="text-align: center;" ><?php echo system_number_money_format($bet['amount']); ?> </td>
                            <?php }elseif($bet['scoring_type'] == 4){ ?>
                                <td style="text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[3]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo system_number_money_format($bet['amount']); ?> </td>
                            <?php }elseif($bet['scoring_type'] == 5){ ?>
                                <td style="text-align: center" ><?php echo $betParts[0]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[1]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[2]['number']; ?> </td>
                                <td style="text-align: center" ><?php echo $betParts[3]['number']; ?> </td>
                                <td style="text-align: center;" ><?php echo system_number_money_format($bet['amount']); ?>AO </td>
                            <?php } ?>
                            <?php $stmtPrize->execute(array($ticketID, $bet['id']));
                            $prize = $stmtPrize->fetch()?>
                            <?php if($prize['finalPrize'] != ''){?>
                                <td style="text-align: center; font-weight: bold"><?php echo system_number_money_format($prize['finalPrize'])?></td>
                                <?php $totalPremios = $totalPremios + $prize['finalPrize'];
                                array_push($prizeBets, $bet['id']);
                                array_push($prizeAmount, $prize['finalPrize']);
                                $showPayButton = true;?>
                            <?php }else{?>
                                <td style="text-align: center"><?php echo system_number_money_format(0)?></td>
                            <?php }?>
                        </tr>
                            <?php endforeach?>
                            <?php }else{?>
                                <center>Ticket does not exist</center>
                            <?php }?>
                        </tr>
                        <?php if($stmtGetTicketInfo->rowCount() > 0){?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: center">Total Prize</td>
                            <td style="text-align: center"><?php echo system_number_money_format($totalPremios)?></td>
                        </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <?php //if($_SESSION['NivelUsuario'] ==  3):?>
                        <?php if($showPayButton == true):?>
                           <?php $stmtComprobarPago->execute(array($ticketID));
                                 $pagado = $stmtComprobarPago->fetch();?>
                            <?php if($stmtComprobarPago->rowCount() == 0){?>
                              <!--<button type="button" id="btnSearch" class="btn btn-success" style="width 100pxs; margin-top: 5px; text-align: center; font-size: 34px" onclick="makePayment()">PAY</button>-->
                                <label style="font-size: 25px">Noy payed yet</label>
                            <?php }else{?>
                              <label style="font-size: 25px">Pay by <?php echo '<span style="font-weight: bold">' . $pagado['NombreUsuario'] . '</span> at ' . system_date_format($pagado['pay_at'])?></label>
                            <?php }?>
                        <?php endif ?>
                    <?php //endif ?>

                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>

<script>

    function makePayment(){
        var pay = confirm("Mark this ticket as payed? \nNOTE: You'll take all the responsabilities of this payment and this action is not reversible");

        if(pay == true){
            var prizeBets = new Array();
            var prizeAmount = new Array();
            <?php for($i = 0; $i < sizeof($prizeBets); $i++){ ?>
               prizeBets.push(<?php echo $prizeBets[$i]?>);
               prizeAmount.push(<?php echo $prizeAmount[$i]?>);
            <?php } ?>
            var parametros = {
                "TicketID" : <?php echo $ticketID?>,
                "TicketBetPart" : prizeBets,
                "Prize" : prizeAmount
            };
            $.ajax({
                data : parametros,
                url: 'section_tickets_search_data.php',
                type: 'post',
                beforeSend: function(){
                    $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
                },
                success: function(response){
                    $("#divTicketInfo").html(response);
                    $("#btnSearch").click();
                }
            });


        }else{

        }


    }//FIN makePayment

</script>