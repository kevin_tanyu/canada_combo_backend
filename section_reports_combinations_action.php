<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 04:48 PM
 */

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//CAPTURA LA FECHAS DEL FORM
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');

$dateFrom = date("Y-m-d 00:01", strtotime($dateFrom));
$dateTo = date("Y-m-d 23:59", strtotime($dateTo));

$today = date('Y-m-d H:i:s');
//SI LA FECHA EN DONDE DONDE EMPIEZA, CAPTURA LA HORA ACTUAL
if($dateFrom == $today){
    $dateFrom = date('Y-m-d H:i:s');
}

/*PARA CAPTURAR EL ULTIMO SORTEO ANTERIOR A LA FECHA ESCOGIDA*/
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($dateFrom));
$LastSorteo = $stmtGetLastSorteo->fetch();

if($stmtGetLastSorteo->rowCount() > 0){
    $dateFrom = $LastSorteo['FechayHora'];
}else{

    /*SI NO HAY SORTEO ANTES DE LA FECHA INGRESADA CAPTURA EL SIGUIENTE MAS PROXIMO*/
  /*  $sqlGetNextSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora > ?
                     ORDER BY FechayHora ASC LIMIT 1";
    $stmtGetNextSorteo = $pdoConn->prepare($sqlGetNextSorteo);
    $stmtGetNextSorteo->execute(array($dateFrom));
    $NextSorteo = $stmtGetNextSorteo->fetch();
    $dateFrom = $NextSorteo['FechayHora'];*/

}//FIN if/else
/*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
if($dateTo != $today){
    $stmtGetLastSorteo->execute(array($dateTo));
    $LastSorteo = $stmtGetLastSorteo->fetch();


    if($stmtGetLastSorteo->rowCount() > 0){
        $dateTo = $LastSorteo['FechayHora'];
    }
}
/*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
if($dateFrom == $dateTo){
    $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
    $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
}

$nivelUsuario = $_SESSION['NivelUsuario'];
$userID = $_SESSION['IDUsuario'];

/******************************REPORTE COMBINACIONES*************************************************************/
$combinacionesArray = array();
$montosArray = array();

try{

    if($nivelUsuario == 1){//HOUSE

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }elseif($nivelUsuario == 2){//AGENT

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }elseif($nivelUsuario == 3){//STORE

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre =". $userID ."  )
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }//FIN IF/ELSE


    $stmtTiquetes = $pdoConn->prepare($sqlTiquetes);
    $stmtTiquetes->execute(array($dateFrom, $dateTo));
    $Tiquetes = $stmtTiquetes->fetchAll(PDO::FETCH_ASSOC);

    /************QUERY BET_POR_TICKET***************/
    $sqlApuestas = "SELECT *
                    FROM  Ticket_Bet
                    WHERE  ticketID  = :ticket_id";
    $stmtApuestas = $pdoConn->prepare($sqlApuestas);
    /**********************************************/

    /************QUERY COMBINACION BET_POR_TICKET***************/
    $sqlCombinaciones = "SELECT *
                         FROM Ticket_Bet_Part
                         WHERE ticketBetID = :apuesta_id
                         ORDER BY orderNumber ASC";
    $stmtCombinaciones = $pdoConn->prepare($sqlCombinaciones);
    /**********************************************/

    $Pick1 = array();
    $MontoPick1 = array();

    $Pick2 = array();
    $MontoPick2 = array();

    $Pick3 = array();
    $MontoPick3 = array();

    $Pick4 = array();
    $MontoPick4 = array();

    $Pick5 = array();
    $MontoPick5 = array();

    foreach($Tiquetes as $tiquete){
        $stmtApuestas->execute(array(':ticket_id' => $tiquete['id']));
        $Apuestas = $stmtApuestas->fetchAll(PDO::FETCH_ASSOC);

        foreach($Apuestas as $apuesta){
            $stmtCombinaciones->execute(array(':apuesta_id' => $apuesta['id']));
            $Combinaciones = $stmtCombinaciones->fetchAll(PDO::FETCH_ASSOC);

            $combinacionTemp = array();
            foreach($Combinaciones as $combinacion){
                $numero = $combinacion['number'];
                array_push($combinacionTemp, $numero);
            }//FIN FOREACH $Combinaciones

            $existeCombi = false;
            if($apuesta['scoring_type'] == 1){
                if(sizeof($Pick1) == 0){
                    array_push($Pick1, $combinacionTemp);
                    array_push($MontoPick1, $apuesta['amount']);
                }else{
                    for($i = 0; $i<sizeof($Pick1); $i++){

                        if($combinacionTemp[0] == $Pick1[$i][0]){
                            $MontoPick1[$i] = $MontoPick1[$i] + $apuesta['amount'];
                            $i = sizeof($Pick1);//Salirse del for porque enconrtro  la combinacion
                            $existeCombi = true;
                        }//Fin if

                    }//Fin for
                    if($existeCombi == false){
                        array_push($Pick1, $combinacionTemp);
                        array_push($MontoPick1, $apuesta['amount']);
                    }
                }//Fin if/else

            }elseif($apuesta['scoring_type'] == 2){

                if(sizeof($Pick2) == 0){
                    array_push($Pick2, $combinacionTemp);
                    array_push($MontoPick2, $apuesta['amount']);
                }else{
                    for($i = 0; $i<sizeof($Pick2); $i++){

                        if($combinacionTemp[0] == $Pick2[$i][0] && $combinacionTemp[1] == $Pick2[$i][1]){
                            $MontoPick2[$i] = $MontoPick2[$i] + $apuesta['amount'];
                            $i = sizeof($Pick2);//Salirse del for porque enconrtro  la combinacion
                            $existeCombi = true;
                        }//Fin if

                    }//Fin for
                    if($existeCombi == false){
                        array_push($Pick2, $combinacionTemp);
                        array_push($MontoPick2, $apuesta['amount']);
                    }

                }//Fin if/else

            }elseif($apuesta['scoring_type'] == 3){
                if(sizeof($Pick3) == 0){
                    array_push($Pick3, $combinacionTemp);
                    array_push($MontoPick3, $apuesta['amount']);
                }else{
                    for($i = 0; $i<sizeof($Pick3); $i++){

                        if($combinacionTemp[0] == $Pick3[$i][0] && $combinacionTemp[1] == $Pick3[$i][1] && $combinacionTemp[2] == $Pick3[$i][2]){
                            $MontoPick3[$i] = $MontoPick3[$i] + $apuesta['amount'];
                            $i = sizeof($Pick3);//Salirse del for porque enconrtro  la combinacion
                            $existeCombi = true;
                        }//Fin if

                    }//Fin for
                    if($existeCombi == false){
                        array_push($Pick3, $combinacionTemp);
                        array_push($MontoPick3, $apuesta['amount']);
                    }
                }//Fin if/else

            }elseif($apuesta['scoring_type'] == 4){
                if(sizeof($Pick4) == 0){
                    array_push($Pick4, $combinacionTemp);
                    if($apuesta['scoring_type'] == 5){
                        array_push($MontoPick4, ($apuesta['amount']));
                    }else{
                        array_push($MontoPick4, $apuesta['amount']);
                    }

                }else{
                    for($i = 0; $i<sizeof($Pick4); $i++){

                        if($combinacionTemp[0] == $Pick4[$i][0] && $combinacionTemp[1] == $Pick4[$i][1] && $combinacionTemp[2] == $Pick4[$i][2] && $combinacionTemp[3] == $Pick4[$i][3]){
                            $MontoPick4[$i] = $MontoPick4[$i] + ($apuesta['amount']);
                            $i = sizeof($Pick4);//Salirse del for porque enconrtro  la combinacion
                            $existeCombi = true;
                        }//Fin if

                    }//Fin for

                    if($existeCombi == false){
                        array_push($Pick4, $combinacionTemp);
                        if($apuesta['scoring_type'] == 5){
                            array_push($MontoPick4, ($apuesta['amount']-1));
                        }else{
                            array_push($MontoPick4, $apuesta['amount']);
                        }
                    }
                }//Fin if/else

            }elseif($apuesta['scoring_type'] == 5){
                if(sizeof($Pick5) == 0){
                    array_push($Pick5, $combinacionTemp);
                    array_push($MontoPick5, $apuesta['amount']);

                }else{
                    for($i = 0; $i<sizeof($Pick5); $i++){

                        if($combinacionTemp[0] == $Pick5[$i][0] && $combinacionTemp[1] == $Pick5[$i][1] && $combinacionTemp[2] == $Pick5[$i][2] && $combinacionTemp[3] == $Pick5[$i][3]){
                            $MontoPick5[$i] = $MontoPick5[$i] + ($apuesta['amount']);
                            $i = sizeof($Pick5);//Salirse del for porque enconrtro  la combinacion
                            $existeCombi = true;
                        }//Fin if

                    }//Fin for

                    if($existeCombi == false){
                        array_push($Pick5, $combinacionTemp);
                        array_push($MontoPick5, $apuesta['amount']);
                    }
                }//Fin if/else

            }//Fin elseif



        }//FIN FOREACH $Apuestas


    }//FIN FOREACH $Tiquetes

   /* $total = 0;
    $topCombiarray = array();
    $topMontoarray = array();

    $cantidadArray = sizeof($montosArray);*/

   /* for($p = 0; $p < $cantidadArray; $p++){
        $valor = max($montosArray);
        $pos = array_search($valor, $montosArray);
        array_push($topCombiarray, $combinacionesArray[$pos]);
        array_push($topMontoarray, $montosArray[$pos]);
        unset($montosArray[$pos]);
        unset($combinacionesArray[$pos]);
    }*/

    $totalPick1 = 0;
    $totalPick2 = 0;
    $totalPick3 = 0;
    $totalPick4 = 0;
    $totalPick5 = 0;

}catch (Exception $e){
    echo "A error has ocurred. Please try again.";
}


?>

</br></br>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
    <div class="modal-dialog" style="width: 770px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Ticket Info</h4>
            </div>
            <div class="modal-body" style="width: 770px;">
                <div id="divTicketInfo"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #CED8F6">
            <span style="font-size: 20px; font-weight: bold">Pick 1</span> Combinations Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales1">
                    <thead>
                        <th>1st</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($Pick1); $i++):?>
                        <?php $temp =  $Pick1[$i]?>
                        <tr style="text-align: center">
                            <td><?php echo $temp[0];?></td>
                            <td><?php echo system_number_money_format($MontoPick1[$i])?></td>
                            <?php $totalPick1 = $totalPick1 + $MontoPick1[$i]?>
                            <!--<td><button type="button" class="btn btn-link" onclick="mostrarTiquetes(<?php //echo $temp[0];?>, <?php //echo $temp[1];?>);">
                                    See Details</button></td>-->
                            <td><button type="button" class="btn btn-link" onclick="ticketPick1(<?php echo $temp[0];?>)" data-toggle="modal" data-target="#myModal">
                                    See Details</button></td>
                        </tr>
                    <?php endfor?>
                    <tr style="text-align: center; font-weight: bold">
                        <td>Total</td>
                        <td><?php echo system_number_money_format($totalPick1)?></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #A9BCF5">
            <span style="font-size: 20px; font-weight: bold">Pick 2</span> Combinations Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales2">
                    <thead>
                    <th>1st</th>
                    <th>2nd</th>
                    <th>Amount</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($Pick2); $i++):?>
                        <?php $temp =  $Pick2[$i]?>
                        <tr style="text-align: center">
                            <td><?php echo $temp[0];?></td>
                            <td><?php echo $temp[1];?></td>
                            <td><?php echo system_number_money_format($MontoPick2[$i])?></td>
                            <?php $totalPick2 = $totalPick2 + $MontoPick2[$i]?>
                            <!--<td><button type="button" class="btn btn-link" onclick="mostrarTiquetes(<?php //echo $temp[0];?>, <?php //echo $temp[1];?>);">
                                    See Details</button></td>-->
                            <td><button type="button" class="btn btn-link" onclick="ticketPick2(<?php echo $temp[0];?>, <?php echo $temp[1];?>)" data-toggle="modal" data-target="#myModal">
                                    See Details</button></td>
                        </tr>
                    <?php endfor?>
                    <tr style="text-align: center; font-weight: bold">
                        <td></td>
                        <td>Total</td>
                        <td><?php echo system_number_money_format($totalPick2)?></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #819FF7">
            <span style="font-size: 20px; font-weight: bold">Pick 3</span> Combinations Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales3">
                    <thead>
                    <th>1st</th>
                    <th>2nd</th>
                    <th>3rd</th>
                    <th>Amount</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($Pick3); $i++):?>
                        <?php $temp =  $Pick3[$i]?>
                        <tr style="text-align: center">
                            <td><?php echo $temp[0];?></td>
                            <td><?php echo $temp[1];?></td>
                            <td><?php echo $temp[2];?></td>
                            <td><?php echo system_number_money_format($MontoPick3[$i])?></td>
                            <?php $totalPick3 = $totalPick3 + $MontoPick3[$i]?>
                            <!--<td><button type="button" class="btn btn-link" onclick="mostrarTiquetes(<?php //echo $temp[0];?>, <?php //echo $temp[1];?>);">
                                    See Details</button></td>-->
                            <td><button type="button" class="btn btn-link" onclick="ticketPick3(<?php echo $temp[0];?>, <?php echo $temp[1];?>, <?php echo $temp[2];?>)" data-toggle="modal" data-target="#myModal">
                                    See Details</button></td>
                        </tr>
                    <?php endfor?>
                    <tr style="text-align: center; font-weight: bold">
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td><?php echo system_number_money_format($totalPick3)?></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #5882FA">
            <span style="font-size: 20px; font-weight: bold">Pick 4</span> Combinations Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales4">
                    <thead>
                    <th>1st</th>
                    <th>2nd</th>
                    <th>3rd</th>
                    <th>4th</th>
                    <th>Amount</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($Pick4); $i++):?>
                        <?php $temp =  $Pick4[$i]?>
                        <tr style="text-align: center">
                            <td><?php echo $temp[0];?></td>
                            <td><?php echo $temp[1];?></td>
                            <td><?php echo $temp[2];?></td>
                            <td><?php echo $temp[3];?></td>
                            <td><?php echo system_number_money_format($MontoPick4[$i])?></td>
                            <?php $totalPick4 = $totalPick4 + $MontoPick4[$i]?>
                            <!--<td><button type="button" class="btn btn-link" onclick="mostrarTiquetes(<?php //echo $temp[0];?>, <?php //echo $temp[1];?>);">
                                    See Details</button></td>-->
                            <td><button type="button" class="btn btn-link" onclick="ticketPick4(<?php echo $temp[0];?>, <?php echo $temp[1];?>, <?php echo $temp[2];?>, <?php echo $temp[3];?>)" data-toggle="modal" data-target="#myModal">
                                    See Details</button></td>
                        </tr>
                    <?php endfor?>
                    <tr style="text-align: center; font-weight: bold">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td><?php echo system_number_money_format($totalPick4)?></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<!-- /.col-lg-6 -->
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #2E64FE">
            <span style="font-size: 20px; font-weight: bold">Any Order</span> Combinations Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-sales5">
                    <thead>
                    <th>1st</th>
                    <th>2nd</th>
                    <th>3rd</th>
                    <th>4th</th>
                    <th>Amount</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($Pick5); $i++):?>
                        <?php $temp =  $Pick5[$i]?>
                        <tr style="text-align: center">
                            <td><?php echo $temp[0];?></td>
                            <td><?php echo $temp[1];?></td>
                            <td><?php echo $temp[2];?></td>
                            <td><?php echo $temp[3];?></td>
                            <td><?php echo system_number_money_format($MontoPick5[$i])?></td>
                            <?php $totalPick5 = $totalPick5 + $MontoPick5[$i]?>
                            <!--<td><button type="button" class="btn btn-link" onclick="mostrarTiquetes(<?php //echo $temp[0];?>, <?php //echo $temp[1];?>);">
                                    See Details</button></td>-->
                            <td><button type="button" class="btn btn-link" onclick="ticketPick5(<?php echo $temp[0];?>, <?php echo $temp[1];?>, <?php echo $temp[2];?>, <?php echo $temp[3];?>)" data-toggle="modal" data-target="#myModal">
                                    See Details</button></td>
                        </tr>
                    <?php endfor?>
                    <tr style="text-align: center; font-weight: bold">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td><?php echo system_number_money_format($totalPick5)?></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->
<div id="details"></div>

</br>

<script>

    $(document).ready(function() {
        $('#dataTables-sales1').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-sales2').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-sales3').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-sales4').DataTable({
            responsive: true
        });
    });

    $(document).ready(function() {
        $('#dataTables-sales5').DataTable({
            responsive: true
        });
    });

    function ticketPick1(numero1){
        var parametros = {
            "fromDate" : '<?php echo $dateFrom;?>',
            "toDate" : '<?php echo $dateTo?>',
            "numero1" : numero1,
            "Type" : 1
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }//Fin ticketPick1

    function ticketPick2(numero1, numero2){
        var parametros = {
            "fromDate" : '<?php echo $dateFrom;?>',
            "toDate" : '<?php echo $dateTo?>',
            "numero1" : numero1,
            "numero2" : numero2,
            "Type" : 2

        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }//Fin ticketPick1

    function ticketPick3(numero1, numero2, numero3){
        var parametros = {
            "fromDate" : '<?php echo $dateFrom;?>',
            "toDate" : '<?php echo $dateTo?>',
            "numero1" : numero1,
            "numero2" : numero2,
            "numero3" : numero3,
            "Type" : 3
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }//Fin ticketPick1



    function ticketPick4(numero1, numero2, numero3, numero4){
        var parametros = {
            "fromDate" : '<?php echo $dateFrom;?>',
            "toDate" : '<?php echo $dateTo?>',
            "numero1" : numero1,
            "numero2" : numero2,
            "numero3" : numero3,
            "numero4" : numero4,
            "Type" : 4
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }

    function ticketPick5(numero1, numero2, numero3, numero4){
        var parametros = {
            "fromDate" : '<?php echo $dateFrom;?>',
            "toDate" : '<?php echo $dateTo?>',
            "numero1" : numero1,
            "numero2" : numero2,
            "numero3" : numero3,
            "numero4" : numero4,
            "Type" : 5
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_combinations_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divTicketInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divTicketInfo").html(response);
            }
        });
    }




</script>
