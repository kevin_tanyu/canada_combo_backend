<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 05:18 PM
 */
session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");


$dateFrom = $_POST['fromDate'];
$dateTo = $_POST['toDate'];
$nivelUsuario = $_SESSION['NivelUsuario'];
$userID = $_SESSION['IDUsuario'];


/*GET TICKET DETAILS*/
$sqlTiqueteDetalle = "SELECT *
                      FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                      WHERE P.id = ?";
$stmtTiquteDetalle = $pdoConn->prepare($sqlTiqueteDetalle);


/******************************REPORTE COMBINACIONES*************************************************************/


try{

    if($nivelUsuario == 1){//HOUSE

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }elseif($nivelUsuario == 2){//AGENT

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }elseif($nivelUsuario == 3){//STORE

        /*******GET VENTA********/
        $sqlTiquetes = "SELECT *
                        FROM Ticket P
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre =". $userID ."  )
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

    }//FIN IF/ELSE

    $stmtTiquetes = $pdoConn->prepare($sqlTiquetes);
    $stmtTiquetes->execute(array($dateFrom, $dateTo));
    $Tiquetes = $stmtTiquetes->fetchAll(PDO::FETCH_ASSOC);

    /************QUERY BET_POR_TICKET***************/
    $sqlApuestas = "SELECT *
                    FROM  Ticket_Bet
                    WHERE  ticketID  = ? AND scoring_type = ? ";
    $stmtApuestas = $pdoConn->prepare($sqlApuestas);

    $sqlApuestasMixed = "SELECT *
                         FROM  Ticket_Bet
                         WHERE  (ticketID  = ? AND scoring_type = ?) OR (ticketID  = ? AND scoring_type = ?)";
    $stmtApuestasMixed = $pdoConn->prepare($sqlApuestasMixed);

    /**********************************************/

    /************QUERY COMBINACION BET_POR_TICKET***************/
    $sqlCombinaciones = "SELECT *
                         FROM Ticket_Bet_Part
                         WHERE ticketBetID = :apuesta_id";
    $stmtCombinaciones = $pdoConn->prepare($sqlCombinaciones);
    /**********************************************/

    $numeroTiquete = Array();
    $amountApuesta = Array();



    foreach($Tiquetes as $tiquete){
            $stmtApuestas->execute(array($tiquete['id'], $_POST['Type']));
            $Apuestas = $stmtApuestas->fetchAll(PDO::FETCH_ASSOC);

        foreach($Apuestas as $apuesta){
            $stmtCombinaciones->execute(array(':apuesta_id' => $apuesta['id']));
            $Combinaciones = $stmtCombinaciones->fetchAll(PDO::FETCH_ASSOC);

            if($_POST['Type'] == 1){
                if($Combinaciones[0]['number'] ==  $_POST['numero1']){

                    /*Guarda Numero de Tiquete y monto de la apuesta*/
                    array_push($numeroTiquete, $tiquete['id']);
                    array_push($amountApuesta, $apuesta['amount']);

                }

            }elseif($_POST['Type'] == 2){
                if($Combinaciones[0]['number'] ==  $_POST['numero1'] && $Combinaciones[1]['number'] ==  $_POST['numero2']){

                    /*Guarda Numero de Tiquete y monto de la apuesta*/
                    array_push($numeroTiquete, $tiquete['id']);
                    array_push($amountApuesta, $apuesta['amount']);

                }

            }elseif($_POST['Type'] == 3){
                if($Combinaciones[0]['number'] ==  $_POST['numero1'] && $Combinaciones[1]['number'] ==  $_POST['numero2'] && $Combinaciones[2]['number'] ==  $_POST['numero3']){

                    /*Guarda Numero de Tiquete y monto de la apuesta*/
                    array_push($numeroTiquete, $tiquete['id']);
                    array_push($amountApuesta, $apuesta['amount']);

                }

            }elseif($_POST['Type'] == 4){
                if($Combinaciones[0]['number'] ==  $_POST['numero1'] && $Combinaciones[1]['number'] ==  $_POST['numero2'] && $Combinaciones[2]['number'] ==  $_POST['numero3'] && $Combinaciones[3]['number'] ==  $_POST['numero4']){

                    /*Guarda Numero de Tiquete y monto de la apuesta*/
                    array_push($numeroTiquete, $tiquete['id']);
                    array_push($amountApuesta, $apuesta['amount']);

                }

            }elseif($_POST['Type'] == 5){
                if($Combinaciones[0]['number'] ==  $_POST['numero1'] && $Combinaciones[1]['number'] ==  $_POST['numero2'] && $Combinaciones[2]['number'] ==  $_POST['numero3'] && $Combinaciones[3]['number'] ==  $_POST['numero4']){

                    /*Guarda Numero de Tiquete y monto de la apuesta*/
                    array_push($numeroTiquete, $tiquete['id']);
                    array_push($amountApuesta, $apuesta['amount']);

                }
            }//FIN else if




        }//FIN FOREACH $Apuestas


    }//FIN FOREACH Tiquetes




}catch (Exception $e){
    echo "A error has ocurred. Please try again.";
}


/******************************FIN REPORTE COMBINACIONES*************************************************************/

?>



        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-details1">
                    <thead>
                    <th>Ticket</th>
                    <th>Seller</th>
                    <?php if($_POST['Type'] == 1 || $_POST['Type'] == 2 || $_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                        <th>1st</th>
                    <?php endif ?>
                    <?php if($_POST['Type'] == 2 || $_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                        <th>2nd</th>
                    <?php endif ?>
                    <?php if($_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                        <th>3rd</th>
                    <?php endif ?>
                    <?php if($_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                        <th>4th</th>
                    <?php endif ?>
                    <th>Amount</th>
                    </thead>
                    <tbody>
                    <?php for($i = 0; $i < sizeof($numeroTiquete); $i++):?>
                        <?php $stmtTiquteDetalle->execute(array($numeroTiquete[$i]));
                              $Ticket = $stmtTiquteDetalle->fetch();?>
                        <tr style="text-align: center">
                            <td><?php echo $numeroTiquete[$i]?></td>
                            <td><?php echo $Ticket['NombreUsuario']?></td>
                            <?php if($_POST['Type'] == 1 || $_POST['Type'] == 2 || $_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                                <td><?php echo $_POST['numero1']?></td>
                            <?php endif ?>
                            <?php if($_POST['Type'] == 2 || $_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                                <td><?php echo $_POST['numero2']?></td>
                            <?php endif ?>
                            <?php if($_POST['Type'] == 3 || $_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                                <td><?php echo $_POST['numero3']?></td>
                            <?php endif ?>
                            <?php if($_POST['Type'] == 4 || $_POST['Type'] == 5): ?>
                                <td><?php echo $_POST['numero4']?></td>
                            <?php endif ?>
                            <td><?php echo system_number_money_format($amountApuesta[$i])?></td>
                        </tr>
                    <?php endfor?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->


<script>
    $(document).ready(function() {
        $('#dataTables-details1').DataTable({
            responsive: true
        });
    });
</script>






