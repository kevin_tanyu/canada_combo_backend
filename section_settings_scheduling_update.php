<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 07/05/15
 * Time: 05:31 PM
 */


session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

/*Captura los valores enviados pore Jquery*/
$fromDate = $_POST['fromDate'];
$toDate = $_POST['toDate'];
$evening = $_POST['evening'];
$eveTime = $_POST['eveTime'];

$sqlVerificarSorteo = "SELECT * FROM SorteosProgramacion
                       WHERE IDSorteoDefinicion = :SorteoDefinicion AND DATE(FechayHora) = :FechayHora";
$stmtVerificarSorteo = $pdoConn->prepare($sqlVerificarSorteo);

$sqlUpdateSorteo = "UPDATE SorteosProgramacion
                    SET FechayHora = :newFecha
                    WHERE DATE(FechayHora) = :oldFecha AND IDSorteoDefinicion = :SorteoDefinicion";
$stmtUpdateStoreo = $pdoConn->prepare($sqlUpdateSorteo);

//PARA LLEVAR EL CONTROL DE LOS SORTEOS CREADOS
$contadorMidday = 0;
$contadorEvening = 0;

//FORMATEO LA FECHA
$fromDate = date("Y-m-d", strtotime($fromDate));
$toDate = date("Y-m-d", strtotime($toDate));

$today = date("Y-m-d");

//VALIDA QUE NO HAY FECHAS ANTERIORES AL DIA Y SI EL A ES MAYOR A DESDE
if($fromDate < $today){
    $fromDate = $today;
}else if($toDate < $today){
    $toDate = $today;
}elseif($fromDate < $today && $toDate < $today){
    $fromDate = $today;
    $toDate = $today;
}elseif($toDate < $fromDate){
    $toDate = $fromDate;
}

while($fromDate <= $toDate){
//PARA COMPROBAR SI CHECKEARON EL CHECKBOX
        $stmtVerificarSorteo->execute(array(':SorteoDefinicion' => 1, 'FechayHora' => $fromDate));
        if($stmtVerificarSorteo->rowCount() == 1){

            $fechayhora = $fromDate . " " . $eveTime;
            $stmtUpdateStoreo->execute(array(':newFecha' => $fechayhora, ':oldFecha' => $fromDate, ':SorteoDefinicion' => 1));
            $contadorEvening++;

        }//FIN IF


    $fromDate = date('Y-m-d', strtotime($fromDate. ' + 1 days'));

}//FIN WHILE

echo   $contadorEvening . " Draw Updated";