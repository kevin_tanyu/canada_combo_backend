<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 03:43 PM
 */
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");


?>



<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Winner Numbers</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

      <div id="dataSorteos">

      </div>


    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->



<script>

    $( document ).ready(function() {
        $.ajax({
            url: 'section_settings_winning_number_form.php',
            type: 'post',
            beforeSend: function(){
                $("#dataSorteos").html("Loading Data...");
            },
            success: function(response){
                $("#dataSorteos").html(response);
            }
        });

    });

</script>



