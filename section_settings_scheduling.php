<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 03:43 PM
 */
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

//$dateFrom = date('m/d/Y', strtotime($dateFrom. ' + 30 days'));

?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Draw Scheduling</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Scheduling Form
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="chkEdit" onclick="editMode()">Edit Mode
                        </label>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Select Day or Range</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rbFechas" id="rbFechas" value="1" checked>
                                        Single
                                        <label>Day&nbsp&nbsp&nbsp</label>
                                        <input type="text" value="<?php echo $dateFrom ?>" id="singleDate" class="datepicker">
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="rbFechas" id="rbFechas" value="2">
                                        Range
                                        <label>From</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                                        <label>To</label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">
                                        <button type="button" class="btn btn-default" onclick="mostrarSorteos(1); return false">Show Draws</button>
                                    </label>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label>Select Time</label>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="chkEvening" value="Evening">Daily Draw
                                            <label>Time</label>
                                                <select id="slcEveningHour">
                                                    <?php for($i = 1; $i < 24; $i++):?>
                                                        <?php if($i == 22){?>
                                                            <option value="<?php echo $i ?>" selected><?php echo $i ?></option>
                                                        <?php }else{ ?>
                                                            <option value="<?php echo $i ?>"><?php echo $i?></option>
                                                        <?php }?>
                                                    <?php endfor?>
                                                </select>
                                            :
                                                <select id="slcEveningTime">
                                                    <?php for($i = 0; $i < 60; $i++):?>
                                                        <?php if($i < 10){?>
                                                            <option value="<?php echo '0' . $i ?>"><?php echo '0' . $i ?></option>
                                                        <?php }else{ ?>
                                                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                        <?php }?>
                                                    <?php endfor?>
                                                </select>
                                        </label>
                                    </div>
                                </div>

                            <button type="button" id="btnCrear" class="btn btn-default" onclick="crearSorteos(); return false">Create Draws</button>
                            <button type="button" id="btnEditar" class="btn btn-default" onclick="actualizaSorteos(); return false" style="visibility: hidden">Edit Draws</button>

                            </br></br>
                            <div id="result"></div>

                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->

        <div id="edit"></div>

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>

    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    function editMode(){
        if($('#chkEdit').is(':checked')){
            $("#btnCrear").css("visibility", "hidden");
            $("#btnEditar").css("visibility", "visible");
        }else{
            $('#btnCrear').css("visibility", "visible");
            $('#btnEditar').css("visibility", "hidden");
        }
    }

    //CREAR SORTEOS
    function crearSorteos(){

        /*Verifica cual radiobutton esta activo y captura las fechas*/
        var fecha = $('input[name="rbFechas"]:checked').val();
        if(fecha == 1){
             var fromDate = $('#singleDate').val();
             var toDate = $('#singleDate').val();
        }else{
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
        }

        var parametros = {
            "fromDate" : fromDate,
            "toDate" : toDate,
            "eveTime" : $('#slcEveningHour').val() + ':' +$('#slcEveningTime').val()
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_scheduling_action.php',
            type: 'post',
            beforeSend: function(){
                $("#result").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#result").html(response);
            }
        });
    }//FIN CREAR SORTEOS

    //ACTUALIZASORTEO
    function actualizaSorteos(){

        /*Verifica cual radiobutton esta activo y captura las fechas*/
        var fecha = $('input[name="rbFechas"]:checked').val();
        if(fecha == 1){
            var fromDate = $('#singleDate').val();
            var toDate = $('#singleDate').val();
        }else{
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
        }

        /*Para saber cuales checks esta marcado*/
        var midday = false;
        var midTime = "0:00";
        var evening = false;
        var eveTime = "0:00";

        /*Captura las horas si esta checkeado*/
        if($('#chkMidday').is(':checked')){
            midday = true;
            midTime = $('#slcMidday').val();
        }

        if($('#chkEvening').is(':checked')){
            evening = true;
            eveTime = $('#slcEveningHour').val() + ':' +$('#slcEveningTime').val();
        }


        var parametros = {
            "fromDate" : fromDate,
            "toDate" : toDate,
            "midday" : midday,
            "midTime" : midTime,
            "evening" : evening,
            "eveTime" : eveTime
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_scheduling_update.php',
            type: 'post',
            beforeSend: function(){
                $("#result").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#result").html(response);
                mostrarSorteos(0);
            }
        });
    }//FIN ACTUALIZASORTEO

    //RECIBE UN PARAMETRO PARA LIMPIAR O NO EL DIV DE RESULT
    function mostrarSorteos(limpia){
        if(limpia == 1){
            $("#result").html("");
        }

        /*Verifica cual radiobutton esta activo y captura las fechas*/
        var fecha = $('input[name="rbFechas"]:checked').val();
        if(fecha == 1){
            var fromDate = $('#singleDate').val();
            var toDate = $('#singleDate').val();
        }else{
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
        }


        var parametros = {
            "fromDate" : fromDate,
            "toDate" : toDate
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_scheduling_display.php',
            type: 'post',
            beforeSend: function(){
                $("#edit").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#edit").html(response);
            }
        });
    }

</script>