<?php
session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//CAPTURA LOS DATOS DEL POST
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');


$dateFrom = date("Y-m-d 00:01", strtotime($dateFrom));
$dateTo = date("Y-m-d 23:59", strtotime($dateTo));

$today = date('Y-m-d');
if($dateFrom == $today){
    $dateFrom = date('Y-m-d H:i:s');
}
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($dateFrom));
$LastSorteo = $stmtGetLastSorteo->fetch();

if($stmtGetLastSorteo->rowCount() > 0){
    $dateFrom = $LastSorteo['FechayHora'];
}

/*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
if($dateTo != $today){
    $stmtGetLastSorteo->execute(array($dateTo));
    $LastSorteo = $stmtGetLastSorteo->fetch();


    if($stmtGetLastSorteo->rowCount() > 0){
        $dateTo = $LastSorteo['FechayHora'];
    }
}
/*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
if($dateFrom == $dateTo){
    $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
    $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
}

/*OTRAS VARIABLES QUE SE ENVIAN POR POST*/
$nivelUsuario = $_SESSION['NivelUsuario'];
$userID = $_SESSION['IDUsuario'];

$topGroup = array();


try{

    $sqlGetUsername = "SELECT NombreUsuario FROM Usuarios WHERE ID = ?";
    $stmtGetUsername = $pdoConn->prepare($sqlGetUsername);

    $sqlGetHijos = "SELECT * FROM Usuarios WHERE IDPadre = ?";
    $stmtGetHijos = $pdoConn->prepare($sqlGetHijos);

    $sqlGetHijosStore = "SELECT * FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre = ?)";
    $stmtGetHijosStore = $pdoConn->prepare($sqlGetHijosStore);

    $sqlGetHijosMachine = "SELECT * FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre = ?))";
    $stmtGetHijosMachine = $pdoConn->prepare($sqlGetHijosMachine);




}catch(Exception $e){
    echo('ERROR');
}


?>

<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            To Collect Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th style="text-align: center">User</th>
                        <?php if($nivelUsuario == 1 || $nivelUsuario == 2):?>
                            <th style="text-align: center">Percentage</th>
                        <?php endif ?>
                        <th style="text-align: center">Sales</th>
                        <th style="text-align: center">Payment</th>
                        <th style="text-align: center">To Collect</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        //GET HIJOS DEL USUARIO
                        $stmtGetHijos->execute(array($userID));
                        //SUMA TOTALES
                        $totalSales = 0;
                        $totalPayment = 0;
                        $toCollect = 0;?>
                        <?php if($nivelUsuario == 1)://HOUSE ?>
                        <?php
                            $agents = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);
                            foreach($agents as $agent):?>
                            <?php
                                $sqlSales = "SELECT SUM(P.total) as 'total'
                                             FROM Ticket P
                                             WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent['ID'] ."  )))
                                             AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                                $sqlPayment = "SELECT SUM(TP.prize) as 'total'
                                               FROM Ticket_Payment TP
                                               WHERE TP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent['ID'] ."  )))
                                               AND TP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                                $stmtGetSales = $pdoConn->prepare($sqlSales);
                                $stmtGetSales->execute();
                                $Sales = $stmtGetSales->fetch();


                                $stmtGetPayment = $pdoConn->prepare($sqlPayment);
                                $stmtGetPayment->execute();
                                $Payment = $stmtGetPayment->fetch();
                            ?>
                            <tr style="text-align: center">
                                <td><?php echo $agent['NombreUsuario'];?></td>
                                <td><?php echo $agent['Porcentaje'];?>%</td>
                                <td><?php echo system_number_money_format($Sales['total'])?></td>
                                <?php if($Payment['total'] < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }?>
                                <?php $balance = $Sales['total'] - $Payment['total'];
                                      $totalSales = $totalSales + $Sales['total'];
                                      $totalPayment = $totalPayment + $Payment['total'];
                                      $toCollect = $toCollect + $balance;
                                if($balance < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($balance)?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($balance)?></td>
                                <?php }?>
                            </tr>
                        <?php endforeach?>
                            <tr style="text-align: center; font-weight: bold">
                                <td>Totals</td>
                                <?php if($nivelUsuario == 1 || $nivelUsuario == 2):?>
                                    <td> </td>
                                <?php endif ?>
                                    <td><?php echo system_number_money_format($totalSales)?></td>
                                <?php if($totalPayment < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($totalPayment)?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($totalPayment)?></td>
                                <?php }?>
                                <?php if($toCollect < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($toCollect)?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($toCollect)?></td>
                                <?php }?>
                            </tr>
                    <?php endif//HOUSE?>
                    <?php if($nivelUsuario == 2)://AGENT?>
                        <?php
                        $stores = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);
                        foreach($stores as $store):?>
                            <?php
                            $sqlSales = "SELECT SUM(P.total) as 'total'
                                         FROM Ticket P
                                         WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store['ID'] ."  ))
                                         AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                            $sqlPayment = "SELECT SUM(TP.prize) as 'total'
                                           FROM Ticket_Payment TP
                                           WHERE TP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store['ID'] ."  ))
                                           AND TP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                            $stmtGetSales = $pdoConn->prepare($sqlSales);
                            $stmtGetSales->execute();
                            $Sales = $stmtGetSales->fetch();

                            $stmtGetPayment = $pdoConn->prepare($sqlPayment);
                            $stmtGetPayment->execute();
                            $Payment = $stmtGetPayment->fetch();
                            ?>
                            <tr style="text-align: center">
                                <td><?php echo $store['NombreUsuario'];?></td>
                                <td><?php echo $store['Porcentaje'];?>%</td>
                                <td><?php echo system_number_money_format($Sales['total'])?></td>
                                <?php if($Payment['total'] < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }?>
                                <?php $balance = $Sales['total'] - $Payment['total'];
                                $totalSales = $totalSales + $Sales['total'];
                                $totalPayment = $totalPayment + $Payment['total'];
                                $toCollect = $toCollect + $balance;
                                if($balance < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($balance)?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($balance)?></td>
                                <?php }?>
                            </tr>
                        <?php endforeach?>
                        <tr style="text-align: center; font-weight: bold">
                            <td>Totals</td>
                            <?php if($nivelUsuario == 1 || $nivelUsuario == 2):?>
                                <td> </td>
                            <?php endif ?>
                            <td><?php echo system_number_money_format($totalSales)?></td>
                            <?php if($totalPayment < 0){?>
                                <td style="color: red"><?php echo system_number_money_format($totalPayment)?></td>
                            <?php }else{?>
                                <td><?php echo system_number_money_format($totalPayment)?></td>
                            <?php }?>
                            <?php if($toCollect < 0){?>
                                <td style="color: red"><?php echo system_number_money_format($toCollect)?></td>
                            <?php }else{?>
                                <td><?php echo system_number_money_format($toCollect)?></td>
                            <?php }?>
                        </tr>
                    <?php endif//AGENT?>
                    <?php if($nivelUsuario == 3)://STORE?>
                        <?php
                        $machines = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);
                        foreach($machines as $machine):?>
                            <?php
                            $sqlSales = "SELECT SUM(P.total) as 'total'
                                         FROM Ticket P
                                         WHERE P.usuarioID =". $machine['ID'] ."
                                         AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                            $sqlPayment = "SELECT SUM(TP.prize) as 'total'
                                           FROM Ticket_Payment TP
                                           WHERE TP.pay_by = ". $machine['ID'] ."
                                           AND TP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";
                            $stmtGetSales = $pdoConn->prepare($sqlSales);
                            $stmtGetSales->execute();
                            $Sales = $stmtGetSales->fetch();

                            $stmtGetPayment = $pdoConn->prepare($sqlPayment);
                            $stmtGetPayment->execute();
                            $Payment = $stmtGetPayment->fetch();
                            ?>
                            <tr style="text-align: center">
                                <td><?php echo $machine['NombreUsuario'];?></td>
                                <td><?php echo system_number_money_format($Sales['total'])?></td>
                                <?php if($Payment['total'] < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($Payment['total'])?></td>
                                <?php }?>
                                <?php $balance = $Sales['total'] - $Payment['total'];
                                $totalSales = $totalSales + $Sales['total'];
                                $totalPayment = $totalPayment + $Payment['total'];
                                $toCollect = $toCollect + $balance;
                                if($balance < 0){?>
                                    <td style="color: red"><?php echo system_number_money_format($balance)?></td>
                                <?php }else{?>
                                    <td><?php echo system_number_money_format($balance)?></td>
                                <?php }?>
                            </tr>
                        <?php endforeach?>
                        <tr style="text-align: center; font-weight: bold">
                            <td>Totals</td>
                            <?php if($nivelUsuario == 1 || $nivelUsuario == 2):?>
                                <td></td>
                            <?php endif ?>
                            <td><?php echo system_number_money_format($totalSales)?></td>
                            <?php if($totalPayment < 0){?>
                                <td style="color: red"><?php echo system_number_money_format($totalPayment)?></td>
                            <?php }else{?>
                                <td><?php echo system_number_money_format($totalPayment)?></td>
                            <?php }?>
                            <?php if($toCollect < 0){?>
                                <td style="color: red"><?php echo system_number_money_format($toCollect)?></td>
                            <?php }else{?>
                                <td><?php echo system_number_money_format($toCollect)?></td>
                            <?php }?>
                        </tr>
                    <?php endif//STORE?>

                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-6 -->



