<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 10:40 AM
 */
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('Y-m-d H:i:s');
$dateTo = date('Y-m-d 23:59');

$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($dateFrom));
$LastSorteo = $stmtGetLastSorteo->fetch();

if($stmtGetLastSorteo->rowCount() > 0){
    $dateFrom = $LastSorteo['FechayHora'];
}

$nivelUsuario = $_SESSION['NivelUsuario'];
$userID = $_SESSION['IDUsuario'];


/**/


if($nivelUsuario == 1){

    $sqlGetSales = "SELECT SUM(P.total) as 'total'
                    FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                    WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                    AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";

}elseif($nivelUsuario == 2){

    $sqlGetSales = "SELECT SUM(P.total) as 'total'
                    FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                    WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  )))
                    AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";

}elseif($nivelUsuario == 3){

    $sqlGetSales = "SELECT SUM(P.total) as 'total'
                    FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                    WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre = ". $userID ."  )
                     AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'";


}

$stmtTotalTiquetes = $pdoConn->prepare($sqlGetSales);
$stmtTotalTiquetes->execute();
$totalDia = $stmtTotalTiquetes->fetch();


?>

<script>
    $( document ).ready(function() {

      /*  var parametrosCombi = {
            "fromDate" : <?php //echo "'" . $dateFrom2 . "'" ?>,
            "toDate" : <?php //echo "'" . $dateTo2 . "'"?>
        };
        $.ajax({
            data : parametrosCombi,
            url: 'section_reports_combinations_action.php',
            type: 'post',
            beforeSend: function(){
                // $("#divCombinations").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divCombinations").html(response);
            }
        });*/

    });

</script>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-usd fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo system_number_money_format($totalDia['total'])?></div>
                                <div>Total Sales</div>
                            </div>
                        </div>
                    </div>
                    <a href="section_reports_sales.php">
                        <div class="panel-footer">
                            <span class="pull-left">View Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="divCombinations">
        </div>

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->



