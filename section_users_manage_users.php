<?php
session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Manage Users</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div id="divUserGroup">

        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>

    $( document ).ready(function() {
        $.ajax({
            url: 'section_users_manage_users_form.php',
            type: 'post',
            beforeSend: function(){
                $("#divUserGroup").html("Loading Data...");
            },
            success: function(response){
                $("#divUserGroup").html(response);
            }
        });

    });

</script>