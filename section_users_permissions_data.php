<?php

session_start();

include("config.ini.php");
include("conectadb.php");

$permissionUser = $_POST['permissionUser'];
$userId = $_POST['userID'];

try{

    /*SQL ACCIONES*/
    $sqlGetActions = "SELECT M.MenuNombre, MA.id, MA.Accion
                      FROM Menus M JOIN Menus_Accion MA ON M.id = MA.idMenu";
    $stmtGetActions = $pdoConn->prepare($sqlGetActions);
    $stmtGetActions->execute();
    $actions = $stmtGetActions->fetchAll(PDO::FETCH_ASSOC);

    /*SQL VERIFICAR SI EXISTE EL PERMISO*/
    $slqCheckPermission = "SELECT * FROM Menus_Accion_Usuarios
                           WHERE idAccion = ? AND usuarioID = ?";
    $stmtCheckPermission = $pdoConn->prepare($slqCheckPermission);

    /*SQL AGREGAR EL PERMISO*/
    $sqlInsertPermission = "INSERT INTO Menus_Accion_Usuarios(idAccion, usuarioID, activo)
                            VALUES(?,?,?)";
    $stmtInsertPermission = $pdoConn->prepare($sqlInsertPermission);

    /*SQL ACTUALIZAR EL PERMISO*/
    $sqlUpdatePermission = "UPDATE Menus_Accion_Usuarios
                            SET activo = :activo
                            WHERE idAccion = :accionID AND usuarioID = :usuarioID";
    $stmtUpdatePermission = $pdoConn->prepare($sqlUpdatePermission);

    $arrayCont = 0;
    foreach($actions as $action){
        $stmtCheckPermission->execute(array($action['id'], $userId));

        if($stmtCheckPermission->rowcount() == 0){
            $stmtInsertPermission->execute(array($action['id'], $userId, $permissionUser[$arrayCont]));
        }else{
            $stmtUpdatePermission->execute(array(':activo' => $permissionUser[$arrayCont], ':accionID' => $action['id'], ':usuarioID' => $userId));
        }//FIN if/else

        $arrayCont++;

    }//Fin foreach

    echo '<p class="text-success">Permission Updated</p>';

}catch(Exception $e){
    echo '<p class="text-danger">ERROR, Please try again</p>';
}

?>