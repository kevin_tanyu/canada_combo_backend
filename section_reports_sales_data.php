<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

$accion = $_POST['Accion'];
$userID = $_POST['userID'];
$nivelUsuario = $_SESSION['NivelUsuario'];

try{
    /************LOAD DROPDOWN***************************/
    if($accion == 'Agent'){

   /*     if($nivelUsuario == 1){
            $sqlAgent = "SELECT * FROM Usuarios WHERE NivelUsuario = 2";
            $stmtUsers = $pdoConn->prepare($sqlAgent);
            $stmtUsers->execute(array());
        }else{*/
            $sqlAgent = "SELECT * FROM Usuarios WHERE NivelUsuario = 2 AND IDPadre = ?";
            $stmtUsers = $pdoConn->prepare($sqlAgent);
            $stmtUsers->execute(array($userID));
       // }

        $users = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);

        if($stmtUsers->rowCount() > 0){
            $response =
               "</br>Agent: &nbsp&nbsp&nbsp&nbsp<select style='width: 230px' id='slcAgent' onchange='getStoreGroup(this.value), getMachineGroup(this.value)'>
                <option value='0'>All..</option>";
            foreach( $users as  $user){

                $response = $response .    "<option value='" . $user['ID'] . "'>" . $user['NombreUsuario'] . "</option>";
            }

            $response = $response . "</select>";
        }else{
            $response = " ";
        }


    }//Fin if
    elseif($accion == 'Store'){

      /*  if($nivelUsuario == 1){
            $sqlHouse = "SELECT * FROM Usuarios WHERE NivelUsuario = 3";
            $stmtUsers = $pdoConn->prepare($sqlHouse);
            $stmtUsers->execute(array());
        }else{*/
            $sqlHouse = "SELECT * FROM Usuarios WHERE NivelUsuario = 3 AND IDPadre = ?";
            $stmtUsers = $pdoConn->prepare($sqlHouse);
            $stmtUsers->execute(array($userID));
       // }

        $users = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);

        if($stmtUsers->rowCount() > 0){
            $response =
                " </br>Store: &nbsp&nbsp&nbsp&nbsp&nbsp<select style='width: 230px' id='slcStore' onchange='getMachineGroup(this.value)'>
                  <option value='0'>All..</option>";
            foreach( $users as  $user){

                $response = $response .    "<option value='" . $user['ID'] . "'>" . $user['NombreUsuario'] . "</option>";
            }

            $response = $response . "</select>";
        }else{
            $response = " ";
        }


    }//Fin if
    elseif($accion == 'Machine'){

      /*  if($nivelUsuario == 1){
            $sqlMachine = "SELECT * FROM Usuarios WHERE NivelUsuario = 4";
            $stmtUsers = $pdoConn->prepare($sqlMachine);
            $stmtUsers->execute(array());
        }else{*/
            $sqlMachine = "SELECT * FROM Usuarios WHERE NivelUsuario = 4 AND IDPadre = ?";
            $stmtUsers = $pdoConn->prepare($sqlMachine);
            $stmtUsers->execute(array($userID));
        //}

        $users = $stmtUsers->fetchAll(PDO::FETCH_ASSOC);

        if($stmtUsers->rowCount() > 0){
            $response =
                "</br> Machine: <select style='width: 230px' id='slcMachine'>
                 <option value='0'>All..</option>";
            foreach( $users as  $user){

                $response = $response .    "<option value='" . $user['ID'] . "'>" . $user['NombreUsuario'] . "</option>";
            }

            $response = $response . "</select>";
        }else{
            $response = " ";
        }


    }//Fin if
    /************LOAD DROPDOWN***************************/

    echo $response;

}catch (Exception $e){

    echo 'ERROR';

}