<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 04:48 PM
 */

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//CAPTURA LA FECHAS DEL FORM
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');

$dateFrom = date("Y-m-d", strtotime($dateFrom));
$dateTo = date("Y-m-d 23:59", strtotime($dateTo));


$sqlSorteosEvening = "SELECT SP.ID, SD.NombreSorteo, SP.FechayHora
               FROM SorteosProgramacion SP
               JOIN SorteosDefinicion SD
               ON SP.IDSorteoDefinicion = SD.ID
               WHERE SP.IDSorteoDefinicion = 1 AND SP.FechayHora BETWEEN ? AND ?
               ORDER BY SP.FechayHora ASC";

$stmtSorteosEvening = $pdoConn->prepare($sqlSorteosEvening);
$stmtSorteosEvening->execute(array($dateFrom, $dateTo));
$SorteosEvening = $stmtSorteosEvening->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Daily Draws
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Draw</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($SorteosEvening as $sorteo):?>
                            <tr>
                                <td><?php echo $sorteo['NombreSorteo'] ?></td>
                                <td><?php echo system_date_format($sorteo['FechayHora']) ?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->

</div>
<!-- /.row -->