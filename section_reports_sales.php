<?php
/**
 * Created by PhpStorm.
 * User: KTY
 * Date: 06/05/15
 * Time: 03:43 PM
 */

$usuariosHouse = array(1);
$usuariosAgent = array(2);
$usuariosStore = array(3);
$usuariosMachine = array(4);

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

$dateFrom = date('m/d/Y');
$dateTo = date('m/d/Y');

$userID = $_SESSION['IDUsuario'];
$userSonIDArray = Array();
array_push($userSonIDArray, $userID);
/************OBTENER HIJOS POR ID DEL USUARIO****************/
$sqlGetHijos = "SELECT *
                FROM  Usuarios
                WHERE  IDPadre = ?";

$stmtGetHijos = $pdoConn->prepare($sqlGetHijos);
/**************************************************/
$stmtGetHijos->execute(array($userID));
$sonUsers1 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);

$level = 0;

switch($_SESSION['NivelUsuario']){
    case 1:
        $level = 3;
        break;
    case 2:
        $level = 2;
        break;

}

/******************OBTENER LOS HIJOS********************************/
foreach($sonUsers1 as $user1){//AGENT//STORE

    array_push($userSonIDArray, $user1['ID']);

    if($level == 3){
        $stmtGetHijos->execute(array($user1['ID']));
        $sonUsers2 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);

        foreach($sonUsers2 as $user2){//STORE
            array_push($userSonIDArray, $user2['ID']);

            $stmtGetHijos->execute(array($user2['ID']));
            $sonUsers3 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);

            foreach($sonUsers3 as $user3){//MACHINE
                array_push($userSonIDArray, $user3['ID']);
            }//FOREACH

        }//FOREACH
    }//IF
    elseif($level == 2){
        $stmtGetHijos->execute(array($user1['ID']));
        $sonUsers2 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);

        foreach($sonUsers2 as $user2){////MACHINE
            array_push($userSonIDArray, $user2['ID']);

            $stmtGetHijos->execute(array($user2['ID']));
            $sonUsers3 = $stmtGetHijos->fetchAll(PDO::FETCH_ASSOC);


        }//FOREACH
    }

}//FIN FOREACH

/*
$ids     = array(1, 2, 3, 7, 8, 9);
$inQuery = implode(',', array_fill(0, count($ids), '?'));

$message = "";

foreach ($ids as $id)
    $message = $message . $id;
*/

?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sales</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <p>
                <form>
                    <label>FROM</label> <input type="text" value="<?php echo $dateFrom ?>" id="fromDate" class="datepicker">
                    <label>TO</label> <input type="text" value="<?php echo $dateFrom ?>" id="toDate" class="datepicker">


                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosHouse)):?>
                          <div id="divAgent"></div>
                          <div id="divStore"></div>
                          <div id="divMachine"></div>
                    <?php endif ?>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosAgent)):?>
                        <td>
                          <div id="divStore"></div>
                          <div id="divMachine"></div>
                        </td>
                    <?php endif ?>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosStore)):?>
                        <td>
                          <div id="divMachine"></div>
                        </td>
                    <?php endif ?>

                    </br>
                    <?php if (in_array($_SESSION['NivelUsuario'], $usuariosMachine)):?>
                        </br>
                    <?php endif ?>
                    <input type="submit" value="Show" class="button" onclick="getSalesInfoFilter(); return false" style="margin-left: 240px"/>

                </form>

                <br>
                <div id="divSalesInfo">

                    </p>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>

    $( document ).ready(function() {

        var nivelUsuario = <?php echo $_SESSION['NivelUsuario']?>

            if(nivelUsuario == 1){
                getAgentGroup(<?php echo $userID?>);
            }else if(nivelUsuario == 2){
                getStoreGroup(<?php echo $userID?>);
            }else if(nivelUsuario == 3){
                getMachineGroup(<?php echo $userID?>);
            }else if(nivelUsuario == 4){
                getMachineSale();
            }


    });


    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    /***********DROPDOWN METHODS********************/
    function getAgentGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Agent"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divAgent").html("</br>Loading...");
            },
            success: function(response){
                $("#divAgent").html(response);
            }
        });
    }//Fin getAgentGroup

    function getStoreGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Store"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divStore").html("</br>Loading...");
            },
            success: function(response){
                $("#divStore").html(response);
            }
        });
    }//Fin getStoreGroup

    function getMachineGroup(userID){
        var parametros = {
            "userID" : userID,
            "Accion" : "Machine"
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_data.php',
            type: 'post',
            beforeSend: function(){
                $("#divMachine").html("</br>Loading...");
            },
            success: function(response){
                $("#divMachine").html(response);
            }
        });
    }//Fin getMachineGroup

    /***********DROPDOWN METHODS********************/

    //GET USER SALE
    function getMachineSale(){
        var parametros = {
            "userIDs" : Array(0, <?php echo $userID?>),
            "Accion" : 'Machine'
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divSalesInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divSalesInfo").html(response);
            }
        });
    }
    //GET USER SALE


    function getSalesInfoFilter(){
        var agent = -1;
        var store = -1;
        var machine = -1;

        if($('#slcAgent').length){
            agent = $('#slcAgent').val();
        }
        if($('#slcStore').length){
            store = $('#slcStore').val();
        }
        if($('#slcMachine').length){
            machine = $('#slcMachine').val();
        }

        var parametros = {
            "fromDate" : $('#fromDate').val(),
            "toDate" : $('#toDate').val(),
            "Agent" : agent,
            "Store" : store,
            "Machine" : machine,
            "Accion" : 'Filter'
        };
        $.ajax({
            data : parametros,
            url: 'section_reports_sales_display.php',
            type: 'post',
            beforeSend: function(){
                $("#divSalesInfo").html("</br>Processing... Please wait a moment.");
            },
            success: function(response){
                $("#divSalesInfo").html(response);
            }
        });
    }




</script>