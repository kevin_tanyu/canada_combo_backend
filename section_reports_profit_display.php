<?php

session_start();

// Incluye datos generales y conexion a DB
include("config.ini.php");
include("conectadb.php");

//CAPTURA LOS DATOS DEL POST
$dateFrom = isset($_POST['fromDate']) ? $_POST['fromDate'] : date('Y-m-d');
$dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');


$dateFrom = date("Y-m-d 00:01", strtotime($dateFrom));
$dateTo = date("Y-m-d 23:59", strtotime($dateTo));

$today = date('Y-m-d');
if($dateFrom == $today){
    $dateFrom = date('Y-m-d H:i:s');
}
$sqlGetLastSorteo = "SELECT *
                     FROM SorteosProgramacion
                     WHERE FechayHora < ?
                     ORDER BY FechayHora DESC LIMIT 1";
$stmtGetLastSorteo = $pdoConn->prepare($sqlGetLastSorteo);
$stmtGetLastSorteo->execute(array($dateFrom));
$LastSorteo = $stmtGetLastSorteo->fetch();

if($stmtGetLastSorteo->rowCount() > 0){
    $dateFrom = $LastSorteo['FechayHora'];
}else{

    /*SI NO HAY SORTEO ANTES DE LA FECHA INGRESADA CAPTURA EL SIGUIENTE MAS PROXIMO*/
    /*  $sqlGetNextSorteo = "SELECT *
                       FROM SorteosProgramacion
                       WHERE FechayHora > ?
                       ORDER BY FechayHora ASC LIMIT 1";
      $stmtGetNextSorteo = $pdoConn->prepare($sqlGetNextSorteo);
      $stmtGetNextSorteo->execute(array($dateFrom));
      $NextSorteo = $stmtGetNextSorteo->fetch();
      $dateFrom = $NextSorteo['FechayHora'];*/

}//Fin if else

/*SI LA FECHA EN LA QUE TERMINA ES DIFERENTE A LA DE HOY BUSCA EL SORTEO DE ESE DIA O EL EL ULTIMO A LA FECHA Y HORA DEFINIDA*/
if($dateTo != $today){
    $stmtGetLastSorteo->execute(array($dateTo));
    $LastSorteo = $stmtGetLastSorteo->fetch();


    if($stmtGetLastSorteo->rowCount() > 0){
        $dateTo = $LastSorteo['FechayHora'];
    }
}
/*SI LA FECHA DE LOS DOS SORTEOS COINCIDEN LA HORA DE LA FECHA EN LA QUE TERMINA SE SETEA A LAS 23:59 DE ESE DIA */
if($dateFrom == $dateTo){
    $dateTo = isset($_POST['toDate']) ? $_POST['toDate'] . ' 23:59' : date('Y-m-d 23:59');
    $dateTo = date("Y-m-d 23:59", strtotime($dateTo));
}

$nivelUsuario = $_SESSION['NivelUsuario'];
$agent = $_POST['Agent'];
$store = $_POST['Store'];
$machine = $_POST['Machine'];
$userID = $_SESSION['IDUsuario'];

try{

    if($nivelUsuario == 1){//HOUSE
        if($agent == 0){
            /*******GET VENTA********/
            $sqlGetSales = "SELECT SUM(P.total) as 'total'
                            FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                            WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                            AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                            ORDER BY P.usuarioID";


            /******GET PRIZES*****/
            $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                          FROM Ticket_Prizes TP
                          WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                          (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                          AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

            /*******GET PAYMENTS********/
            /* $sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  ))))
                                AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                ORDER BY PTP.pay_by";*/
        }else{
            if($store == 0){
                /*******GET VENTA********/
                $sqlGetSales = "SELECT SUM(P.total) as 'total'
                                FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent ."  )))
                                AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                ORDER BY P.usuarioID";

                /******GET PRIZES*****/
                $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                              FROM Ticket_Prizes TP
                              WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                              (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent ."  )))
                              AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";


                /*******GET PAYMENTS********/
                /* $sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                    FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                    WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $agent ."  )))
                                    AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                    ORDER BY PTP.pay_by";*/

            }else{
                if($machine == 0){
                    /*******GET VENTA********/
                    $sqlGetSales = "SELECT SUM(P.total) as 'total'
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";

                    /******GET PRIZES*****/
                    $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                          FROM Ticket_Prizes TP
                          WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                          (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                          AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

                    /*******GET PAYMENTS********/
                    /*$sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                       FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                       WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                       AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY PTP.pay_by";*/
                }else{

                    /*******GET VENTA********/
                    $sqlGetSales = "SELECT SUM(P.total) as 'total'
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $machine ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";

                    /******GET PRIZES*****/
                    $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                                  FROM Ticket_Prizes TP
                                  WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID = " . $machine ."
                                  AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

                    /*******GET PAYMENTS********/
                    /*$sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                       FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                       WHERE PTP.usuarioID = " . $machine ."
                                       AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY PTP.pay_by";*/
                }//Machine

            }//Store

        }//Agent




    }elseif($nivelUsuario == 2){//AGENT

        if($store == 0){
            /*******GET VENTA********/
            $sqlGetSales = "SELECT SUM(P.total) as 'total'
                        FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  )))
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                        ORDER BY P.usuarioID";

            /******GET PRIZES*****/
            $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                          FROM Ticket_Prizes TP
                          WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                          SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  )))
                          AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";


            /*******GET PAYMENTS********/
            /*$sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                           FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                           WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $userID ."  )))
                           AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                           ORDER BY PTP.pay_by";*/

        }else{
            if($machine == 0){
                /*******GET VENTA********/
                $sqlGetSales = "SELECT SUM(P.total) as 'total'
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";

                /******GET PRIZES*****/
                $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                              FROM Ticket_Prizes TP
                              WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                              SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                              AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

                /*******GET PAYMENTS********/
                /*$sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                   FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                   WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre IN(SELECT ID FROM Usuarios WHERE ID =". $store ."  ))
                                   AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                   ORDER BY PTP.pay_by";*/

            }else{
                /*******GET VENTA********/
                $sqlGetSales = "SELECT SUM(P.total) as 'total'
                                       FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                                       WHERE P.usuarioID = " . $machine ."
                                       AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                       ORDER BY P.usuarioID";

                /******GET PRIZES*****/
                $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                              FROM Ticket_Prizes TP
                              WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID = " . $machine ."
                              AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

                /*******GET PAYMENTS********/
                /* $sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                                    FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                                    WHERE PTP.pay_by = " . $machine ."
                                    AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                                    ORDER BY PTP.pay_by";*/
            }//Machine

        }//Store



    }elseif($nivelUsuario == 3){//STORE

        if($machine == 0){
            /*******GET VENTA********/
            $sqlGetSales = "SELECT SUM(P.total) as 'total'
                        FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                        WHERE P.usuarioID IN (SELECT ID FROM Usuarios WHERE IDPadre =". $userID ."  )
                        AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                         ORDER BY P.usuarioID";

            /******GET PRIZES*****/
            $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                          FROM Ticket_Prizes TP
                          WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID IN
                          SELECT ID FROM Usuarios WHERE IDPadre =". $userID ."  )
                          AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

            /*******GET PAYMENTS********/
            /*$sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                           FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                           WHERE PTP.pay_by IN (SELECT ID FROM Usuarios WHERE IDPadre =". $userID ."  )
                           AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                           ORDER BY PTP.pay_by";*/
        }else{
            /*******GET VENTA********/
            $sqlGetSales = "SELECT SUM(P.total) as 'total'
                            FROM Ticket P JOIN Usuarios U ON P.usuarioID = U.ID
                            WHERE P.usuarioID = " . $machine ."
                            AND P.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                            ORDER BY P.usuarioID";

            /******GET PRIZES*****/
            $sqlPrizes = "SELECT SUM(TP.finalPrize) as 'total'
                          FROM Ticket_Prizes TP
                          WHERE TP.ticketID IN(SELECT id FROM Ticket T WHERE T.usuarioID = " . $machine ."
                          AND T.created_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."')";

            /*******GET PAYMENTS********/
            /* $sqlGetPayments = "SELECT U.NombreUsuario, PTP.ticketID, PTP.pay_at, PTP.prize
                            FROM Ticket_Payment PTP JOIN Usuarios U ON PTP.pay_by = U.ID
                            WHERE PTP.pay_by = " . $machine ."
                            AND PTP.pay_at BETWEEN '" . $dateFrom ."' AND '" . $dateTo ."'
                            ORDER BY PTP.pay_by";*/

        }//Machine




    }//FIN IF/ELSE

    $stmtGetSales = $pdoConn->prepare($sqlGetSales);
    $stmtGetSales->execute();
    $ticketSales = $stmtGetSales->fetch();

    $stmtPrizes = $pdoConn->prepare($sqlPrizes);
    $stmtPrizes->execute();
    $ticketPrizes = $stmtPrizes->fetch();

    $totalBalance = 0;
    $salesTotal = 0;
    $paymentTotal = 0;



}catch(Exception $e){
    echo('ERROR');
}

?>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2 style="color: green"><i class="glyphicon glyphicon-save"></i>    Sales: <label id="lblSales"></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2 style="color: red"><i class="glyphicon glyphicon-open"></i>    Prizes: <label id="lblPrizes"></label></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <h2> <label id="lblBalance"></label></h2>
            </div>
            <div class="panel-footer">

            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<?php /*<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sales Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-sales">
                        <thead>
                        <tr>
                            <th>Seller</th>
                            <th>Date</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($ticketSales as $sale):?>
                        <tr class="gradeA">
                            <td><?php echo $sale['NombreUsuario']?></td>
                            <td><?php echo system_date_format($sale['created_at'])?></td>
                            <td><?php echo system_number_money_format($sale['total'])?></td>
                            <?php $salesTotal = $salesTotal + $sale['total'];?>
                        </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Payments Between <?php echo system_date_format($dateFrom)?> and <?php echo system_date_format($dateTo)?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-payment">
                        <thead>
                        <tr>
                            <th>Pay By</th>
                            <th>Ticket Number</th>
                            <th>Amount</th>
                            <th>Pay At</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($ticketPayment as $payment):?>
                            <tr class="gradeA">
                                <td><?php echo $payment['NombreUsuario']?></td>
                                <td><?php echo $payment['ticketID']?></td>
                                <td><?php echo system_number_money_format($payment['prize'])?></td>
                                <td><?php echo system_date_format($payment['pay_at'])?></td>
                                <?php $paymentTotal = $paymentTotal + $payment['prize'];?>
                            </tr>
                        <?php endforeach?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->*/
?>




<script>
    $(document).ready(function() {
        /*$('#dataTables-sales').DataTable({
         responsive: true
         });
         $('#dataTables-payment').DataTable({
         responsive: true
         });*/

        if(<?php echo $ticketSales['total'] - $ticketPrizes['total']?> >= 0){
            $('#lblBalance').html("<span style='color : green'><i class='fa fa-money'></i>    Balance: <?php echo system_number_money_format($ticketSales['total'] - $ticketPrizes['total'])?></span>");//;
        }else{
            $('#lblBalance').html("<span style='color : red'><i class='fa fa-money'></i>    Balance: <?php echo system_number_money_format($ticketSales['total'] - $ticketPrizes['total'])?></span>");//;
        }

        $('#lblSales').html("<?php echo system_number_money_format($ticketSales['total'])?>");//;
        $('#lblPrizes').html("<?php echo system_number_money_format($ticketPrizes['total'])?>");//;


    });
</script>