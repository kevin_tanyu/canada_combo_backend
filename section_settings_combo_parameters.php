<?php

session_start();
if (!isset($_SESSION['IDUsuario']))
{
    header('Location: login.php');
    exit;
}

include("config.ini.php");
include("conectadb.php");

include("header.php");

/*TIPOS DE HITS*/
$sqlHitsTypes = "SELECT * FROM Scoring_Types";
$stmtHitsTypes = $pdoConn->prepare($sqlHitsTypes);
$stmtHitsTypes->execute();
$hits = $stmtHitsTypes->fetchAll(PDO::FETCH_ASSOC);

/*ACIERTOS*/
$sqlHitsParam = "SELECT * FROM Scoring_Parameters WHERE scoring_type = ?";
$stmtHitsParam = $pdoConn->prepare($sqlHitsParam);

/*LIMITES*/
$sqlLimitsParam = "SELECT * FROM Scoring_Limits WHERE scoring_type = ?";
$stmtLimitsParam = $pdoConn->prepare($sqlLimitsParam);

$arrayBetHits = array();
//PARA OBTENER LOS ID DE LOS HITS
foreach($hits as $hit){
    array_push($arrayBetHits, $hit['id']);
}

/*PARAMETROS DE APUESTAS*/
$sqlBetParam = "SELECT * FROM Bet_Parameters";
$stmtBetParam = $pdoConn->prepare($sqlBetParam);
$stmtBetParam->execute();
$betParam = $stmtBetParam->fetch();

/*PARAMETROS DE TIQUETES*/
$sqlTicketParam = "SELECT * FROM Ticket_Parameters";
$stmtTicketParam = $pdoConn->prepare($sqlTicketParam);
$stmtTicketParam->execute();
$TicketParam = $stmtTicketParam->fetch();


?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quatro Parameters</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Quatro Payment Parameters
                    </div>
                    <!-- .panel-heading -->
                    <div class="panel-body">

                       <table>
                           <tr>
                               <td>
                                   <?php foreach($hits as $hit):?>
                                       <?php $stmtHitsParam->execute(array($hit['id']));
                                       $pays = $stmtHitsParam->fetch();?>
                                       <table>
                                           <tr>
                                               <td>
                                                   <label><?php echo $hit['display_name']?></br></label>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td><input type="text" id="txtHitsPays<?php echo $hit['id']?>" class="form-control" style="width: 120px" value="<?php echo $pays['pays']?>"></td>
                                               <?php if($hit['active'] == 1){?>
                                                   <td><label class="checkbox-inline">&nbsp;&nbsp;&nbsp;
                                                           <input class="checkbox-inline" type="checkbox" id="chkActive<?php echo $hit['id']?>" checked>Active
                                                       </label></td>
                                               <?php }else{?>
                                                   <td><label class="checkbox-inline">&nbsp;&nbsp;&nbsp;
                                                           <input class="checkbox-inline" type="checkbox" id="chkActive<?php echo $hit['id']?>">Active
                                                       </label></td>
                                               <?php }?>
                                           </tr>
                                           </br>
                                       </table>
                                   <?php endforeach?>
                                   </br>
                                   <button type="button" class="btn btn-primary" onclick="updatePayment()">Save Changes</button>
                                   </br></br>
                                   <div id="divResult"></div>
                               </td>
                               <td style="width: 500px; text-align: center; font-weight: bold">
                                   Any Order<br>
                                   4 Different numbers = 200x <br>
                                   One pair = 400x <br>
                                   Two pairs = 800x <br>
                                   3 same numbers = 1200x
                               </td>

                           </tr>
                       </table>


                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Combination Parameters
                    </div>
                    <!-- .panel-heading -->
                    <div class="panel-body">
                        <div class="panel-group" id="accordion">
                            <?php foreach($hits as $hit):?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $hit['id']?>"><?php echo $hit['display_name']?></a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo $hit['id']?>" class="panel-collapse collapse">
                                        <?php $stmtLimitsParam->execute(array($hit['id']));
                                              $limit = $stmtLimitsParam->fetch();?>
                                        <div class="panel-body">
                                            <label>Minimum</label>
                                            </br>
                                            </label><input type="text" id="txtBetMinim<?php echo $limit['id']?>" class="form-control" style="width: 120px" value="<?php echo $limit['mini']?>"></br>
                                            <label>Maximum</label>
                                            </br>
                                            </label><input type="text" id="txtBetMax<?php echo $limit['id']?>" class="form-control" style="width: 120px" value="<?php echo $limit['maxi']?>"></br>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>

                            </br>
                            <button type="button" class="btn btn-primary" onclick="updateBets(<?php echo $hit['id']?>)">Save Changes</button>
                            </br></br>
                            <div id="divResultBet"></div>
                        </div>
                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Ticket Parameters
                    </div>
                    <!-- .panel-heading -->
                    <div class="panel-body">


                        <label>Minimum</label>
                        </br>
                        </label><input type="text" id="txtTicketMinim" class="form-control" style="width: 120px" value="<?php echo $TicketParam['mini']?>"></br>
                        <label>Maximum</label>
                        </br>
                        </label><input type="text" id="txtTicketMax" class="form-control" style="width: 120px" value="<?php echo $TicketParam['maxi']?>"></br>

                        </br>
                        <button type="button" class="btn btn-primary" onclick="updateTicket()">Save Changes</button>
                        </br></br>
                        <div id="divResultTicket"></div>

                    </div>
                    <!-- .panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>


    //Verifica si es numero o no
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }//FIN isNumber


    //UpdatePayment
    function updatePayment(){
        var hitsID = <?php echo json_encode($arrayBetHits); ?>;
        var hitsValue = new Array();
        var hitsActive = new Array();

        for(var z = 0; z < hitsID.length; z++){
            hitsValue[z] = $('#txtHitsPays' + hitsID[z]).val();
            if($('#chkActive' + hitsID[z]).is(':checked')){
                hitsActive[z] = 1;
            }else{
                hitsActive[z] = 0;
            }
        }//ENDFOR


        var parametros = {
            "HitType" : hitsID,
            "HitValue" : hitsValue,
            "HitActive" : hitsActive,
            "Action" : "parameter"
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_combo_parameters_action.php',
            type: 'post',
            beforeSend: function(){
                $("#divResult").html("Processing... Please wait a moment.");
            },
            success: function(response){
                if(response != 'ERROR'){
                    $("#divResult").html(response);
                    $("#divResult").attr('class', 'text-success');
                }else{
                    $("#divResult").html("A error has ocurred. Please try again");
                    $("#divResult").attr('class', 'text-warning');
                }

            }
        });
    }//FIN UpdatePayment


    //UpdateBets
    function updateBets(){
        var hitsID = <?php echo json_encode($arrayBetHits); ?>;
        var arrayLimitsMax = new Array();
        var arrayLimitsMin = new Array();

        for(var z = 0; z < hitsID.length; z++){
            arrayLimitsMax[z] = $('#txtBetMax' + hitsID[z]).val();
            arrayLimitsMin[z] = $('#txtBetMinim' + hitsID[z]).val();
        }//ENDFOR

        var parametros = {
            "HitType" : hitsID,
            "Min" : arrayLimitsMin,
            "Max" : arrayLimitsMax,
            "Action" : "Bet"
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_combo_parameters_action.php',
            type: 'post',
            beforeSend: function(){
                $("#divResultBet").html("Processing... Please wait a moment.");
            },
            success: function(response){
                if(response != 'ERROR'){
                    $("#divResultBet").html(response);
                    $("#divResultBet").attr('class', 'text-success');
                }else{
                    $("#divResultBet").html("A error has ocurred. Please try again");
                    $("#divResultBet").attr('class', 'text-warning');
                }

            }
        });
    }//FIN UpdateBets

    //UpdateTicket
    function updateTicket(){

        var parametros = {
            "Min" : $('#txtTicketMinim').val(),
            "Max" : $('#txtTicketMax').val(),
            "Action" : "Ticket"
        };
        $.ajax({
            data : parametros,
            url: 'section_settings_combo_parameters_action.php',
            type: 'post',
            beforeSend: function(){
                $("#divResultTicket").html("Processing... Please wait a moment.");
            },
            success: function(response){
                if(response != 'ERROR'){
                    $("#divResultTicket").html(response);
                    $("#divResultTicket").attr('class', 'text-success');
                }else{
                    $("#divResultTicket").html("A error has ocurred. Please try again");
                    $("#divResultTicket").attr('class', 'text-warning');
                }

            }
        });
    }//FIN UpdateTicket


</script>